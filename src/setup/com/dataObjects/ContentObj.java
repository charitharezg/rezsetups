package setup.com.dataObjects;

public class ContentObj {
	
	String PackageName;
	String ShortDescription;
	String Packageinclusions;
	String PackageExclusions;
	String packagedescription;
	String SpecialNotes;
	String Thumbnail;
	String ImageGallery;
	String Contacts;
	String TermsandConditions;
	
	
	
	public String getPackageName() {
		return PackageName;
	}
	public void setPackageName(String packageName) {
		PackageName = packageName;
	}
	public String getShortDescription() {
		return ShortDescription;
	}
	public void setShortDescription(String shortDescription) {
		ShortDescription = shortDescription;
	}
	public String getPackageinclusions() {
		return Packageinclusions;
	}
	public void setPackageinclusions(String packageinclusions) {
		Packageinclusions = packageinclusions;
	}
	public String getPackageExclusions() {
		return PackageExclusions;
	}
	public void setPackageExclusions(String packageExclusions) {
		PackageExclusions = packageExclusions;
	}
	public String getPackagedescription() {
		return packagedescription;
	}
	public void setPackagedescription(String packagedescription) {
		this.packagedescription = packagedescription;
	}
	public String getSpecialNotes() {
		return SpecialNotes;
	}
	public void setSpecialNotes(String specialNotes) {
		SpecialNotes = specialNotes;
	}
	public String getThumbnail() {
		return Thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		Thumbnail = thumbnail;
	}
	public String getImageGallery() {
		return ImageGallery;
	}
	public void setImageGallery(String imageGallery) {
		ImageGallery = imageGallery;
	}
	public String getContacts() {
		return Contacts;
	}
	public void setContacts(String contacts) {
		Contacts = contacts;
	}
	public String getTermsandConditions() {
		return TermsandConditions;
	}
	public void setTermsandConditions(String termsandConditions) {
		TermsandConditions = termsandConditions;
	}
	
	
	
	
	


}
