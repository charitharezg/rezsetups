package setup.com.dataObjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotelDetails {
	 String Day;
	 int    noOfDays;
	 String Hotel;
	 //Package Name	Day	Hotel Name	Rate Type	Sales	Occupancy Tax	Energy Tax	Miscellaneous 	Room Type	Bed Type	RatePlan	std Adult Count	Add Adult Count	Child Count

	 
	 
	 String RateType;
	 String SalesTax;
	 String OccupancyTax;
	 String EnergyTax;
	 String MiscellaneousFee;
	 String RoomType;	
	 String BedType;
	 String RatePlan;
	 String stdAdultCount;
	 String AddAdultCount;
	 String ChildCount;
	
	 
	 
	 
	 
	 Map<String, RegionRates> regionRates=new HashMap<String, RegionRates>();


	 
	 
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = Integer.parseInt(noOfDays);
	}
	public Map<String, RegionRates> getRegionRates() {
		return regionRates;
	}
	public void setRegionRates(Map<String, RegionRates> regionRates) {
		this.regionRates = regionRates;
	}
	public String getDay() {
		return Day;
	}
	public void setDay(String day) {
		Day = day;
	}
	public String getHotel() {
		return Hotel;
	}
	public void setHotel(String hotel) {
		Hotel = hotel;
	}
	public String getRoomType() {
		return RoomType;
	}
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}
	public String getStdAdultCount() {
		return stdAdultCount;
	}
	public void setStdAdultCount(String stdAdultCount) {
		this.stdAdultCount = stdAdultCount;
	}
	public String getAddAdultCount() {
		return AddAdultCount;
	}
	public void setAddAdultCount(String addAdultCount) {
		AddAdultCount = addAdultCount;
	}
	public String getChildCount() {
		return ChildCount;
	}
	public void setChildCount(String childCount) {
		ChildCount = childCount;
	}
	
	public String getBedType() {
		return BedType;
	}
	public void setBedType(String bedType) {
		BedType = bedType;
	}
	public String getRatePlan() {
		return RatePlan;
	}
	public void setRatePlan(String ratePlan) {
		RatePlan = ratePlan;
	}
	public String getRateType() {
		return RateType;
	}
	public void setRateType(String rateType) {
		RateType = rateType;
	}
	public String getSalesTax() {
		return SalesTax;
	}
	public void setSalesTax(String salesTax) {
		SalesTax = salesTax;
	}
	public String getOccupancyTax() {
		return OccupancyTax;
	}
	public void setOccupancyTax(String occupancyTax) {
		OccupancyTax = occupancyTax;
	}
	public String getEnergyTax() {
		return EnergyTax;
	}
	public void setEnergyTax(String energyTax) {
		EnergyTax = energyTax;
	}
	public String getMiscellaneousFee() {
		return MiscellaneousFee;
	}
	public void setMiscellaneousFee(String miscellaneousFee) {
		MiscellaneousFee = miscellaneousFee;
	}
	
	
	
	 

}
