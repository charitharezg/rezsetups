package setup.com.dataObjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import setup.com.readers.ReadExcel;



public class assignHotel {





	public List<HotelDetails> readinfo(String excelPath,String PackageName)throws Exception{


		List<HotelDetails> hotellist=new ArrayList<HotelDetails>();
		Map<String , HotelDetails> hotelmap=new TreeMap<String, HotelDetails>(ReadHotelDetails(excelPath,PackageName));
		
		for (Map.Entry<String, HotelDetails> entry : hotelmap.entrySet()) {
			hotellist.add(entry.getValue());
		}
		return hotellist;



	}

	public static void main(String[] args) throws Exception {


		assignHotel ah=new assignHotel();
		//System.out.println("first"+ah.readinfo("MultiRegionOriginTest234.xls","Multi Region Dates").get(0).getHotel());
		//System.out.println("second"+ah.readinfo("MultiRegionOriginTest234.xls","Multi Region Dates").get(0).getRoomType());


		System.out.println(ah.readinfo("MultiRegionOriginTest234.xls","Multi Region Dates").get(0).getRateType());
		


	}	


	public Map<String , HotelDetails> ReadHotelDetails(String excelPath,String PackageName) throws ParseException {
		
	
		
		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(2);
		RateObjectSet rateset=new RateObjectSet();

		Map<String , HotelDetails> objmap=new HashMap<String, HotelDetails>();

		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();
		

		while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
			HotelDetails obj=new HotelDetails();

			obj.setDay(AllValues[1]);
			System.out.println("******"+AllValues[2]);
			obj.setNoOfDays(AllValues[2]);
			obj.setHotel(AllValues[3]);
			obj.setRateType(AllValues[4]);
			
			
			
			
			obj.setSalesTax(AllValues[5]);
			obj.setOccupancyTax(AllValues[6]);
			obj.setEnergyTax(AllValues[7]);
			obj.setMiscellaneousFee(AllValues[8]);
			
			obj.setRoomType(AllValues[9]);
			obj.setBedType(AllValues[10]);
			obj.setRatePlan(AllValues[11]);
			System.out.println("setRoomType" +obj.getRoomType());
			
			
			obj.setStdAdultCount(AllValues[12]);	
			obj.setAddAdultCount(AllValues[13]);
			obj.setChildCount(AllValues[14]);	
			
			System.out.println("obj.getRoomType()"+obj.getRoomType().trim()+"*****");
			obj.setRegionRates(rateset.contractsRead(excelPath, obj.getHotel().trim(), obj.getRoomType().trim()));
			
			
			if (PackageName.equalsIgnoreCase(AllValues[0])) {
				objmap.put(AllValues[1]+"_"+AllValues[2]+"_"+AllValues[3], obj);
				// Key is day_+hotel+room
			}
		}


		return objmap;


	}



}




