package setup.com.dataObjects;

public class costObj {
	
	int Air;
	int Hotel;
	int Program; 
	int TotalCost;
	int AdjustableTotalCost;
	int TotalTaxValue;
	int ProfitMarkupAmount;
	int ProposedSellRate;
	int FinalSellRate;
	int AgentCommissionableAmount;
	int AgentCommissionAmount;
	int AgentNetProfit;
	int AffiliateCommissionableAmount;
	int AffiliateCommissionAmount;
	int AffiliateNetProfit;
	
	
	public int getAir() {
		return Air;
	}
	public void setAir(String air) {
		Air = Integer.valueOf(air);
	}
	public int getHotel() {
		return Hotel;
	}
	public void setHotel(String hotel) {
		Hotel = Integer.valueOf(hotel);
	}
	public int getProgram() {
		return Program;
	}
	public void setProgram(String program) {
		Program = Integer.valueOf(program);
	}
	public int getTotalCost() {
		return TotalCost;
	}
	public void setTotalCost(String totalCost) {
		TotalCost = Integer.valueOf(totalCost);
	}
	public int getAdjustableTotalCost() {
		return AdjustableTotalCost;
	}
	public void setAdjustableTotalCost(String adjustableTotalCost) {
		AdjustableTotalCost = Integer.valueOf(adjustableTotalCost);
	}
	public int getTotalTaxValue() {
		return TotalTaxValue;
	}
	public void setTotalTaxValue(String totalTaxValue) {
		TotalTaxValue = Integer.valueOf(totalTaxValue);
	}
	public int getProfitMarkupAmount() {
		return ProfitMarkupAmount;
	}
	public void setProfitMarkupAmount(String profitMarkupAmount) {
		ProfitMarkupAmount = Integer.valueOf(profitMarkupAmount);
	}
	public int getProposedSellRate() {
		return ProposedSellRate;
	}
	public void setProposedSellRate(String proposedSellRate) {
		ProposedSellRate = Integer.valueOf(proposedSellRate);
	}
	public int getFinalSellRate() {
		return FinalSellRate;
	}
	public void setFinalSellRate(String finalSellRate) {
		FinalSellRate = Integer.valueOf(finalSellRate);
	}
	public int getAgentCommissionableAmount() {
		return AgentCommissionableAmount;
	}
	public void setAgentCommissionableAmount(String agentCommissionableAmount) {
		AgentCommissionableAmount = Integer.valueOf(agentCommissionableAmount);
	}
	public int getAgentCommissionAmount() {
		return AgentCommissionAmount;
	}
	public void setAgentCommissionAmount(String agentCommissionAmount) {
		AgentCommissionAmount = Integer.valueOf(agentCommissionAmount);
	}
	public int getAgentNetProfit() {
		return AgentNetProfit;
	}
	public void setAgentNetProfit(String agentNetProfit) {
		AgentNetProfit = Integer.valueOf(agentNetProfit);
	}
	public int getAffiliateCommissionableAmount() {
		return AffiliateCommissionableAmount;
	}
	public void setAffiliateCommissionableAmount(String affiliateCommissionableAmount) {
		AffiliateCommissionableAmount = Integer.valueOf(affiliateCommissionableAmount);
	}
	public int getAffiliateCommissionAmount() {
		return AffiliateCommissionAmount;
	}
	public void setAffiliateCommissionAmount(String affiliateCommissionAmount) {
		AffiliateCommissionAmount = Integer.valueOf(affiliateCommissionAmount);
	}
	public int getAffiliateNetProfit() {
		return AffiliateNetProfit;
	}
	public void setAffiliateNetProfit(String affiliateNetProfit) {
		AffiliateNetProfit = Integer.valueOf(affiliateNetProfit);
	}
	

}
