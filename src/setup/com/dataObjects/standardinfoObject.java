package setup.com.dataObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import setup.com.packageSetup.ItineraryDetailLoader;
import setup.com.readers.ReadExcel;



public class standardinfoObject {
	
	public String url="";
	public String packageType="";
	public String packageName="";
	public String packageDays="";
	public String ProductIncludes="";
	public String CostBy="";
	public String ChildAllowed="";
	public String StayForm;
	public String StayTo;
	public String BookingFrom;
	public String BookingTO;
	public String OriginDefinition="";
	public String SelectedOriginCountry="";
	public String SelectedOriginCity="";
	public String DestinationCountry="";
	public String DestinationCity="";
	public String Cancellationrefundtype="";
	public String[] CancellationpolicyDateArray;
	public String Cancellationpolicytype="";
	public String[] CancellationChargeArray;
	public String NoshowType="";
	public String Noshowcharge="";
	public String NoofGroupings="";
	public String GroupingHeaders="";
	public String DisplayRanking="";
	public String DCPMType="";	
	public String DCPM	="";
	public String TOPMType="";
	public String TOPM	="";
	public String AFFPMType="";
	public String AFFPM="";
	public String TOCom="";
	public String AffCom="";
	public String TOComType="";
	public String AffComType="";
	public String DepartureBasedOn="";
	public String SelectedDates="";
	
	public String packageDestinationCountry="";
	public String packageDestinationCity="";
	public String packageIsNonRefundable="";
	public String packageApplyifarrivaldateislessthan="";
	public String packageStandardCancellationBasedon="";
	public String packageCancellationValue="";
	public String packageNoShowCancellationBasedon="";
	public String packageNoShowValue="";
	public String packageGrouping="";
	public String MultiHotel="";
	public String multiRoom="";
	public String DCProfitMarkup="";
	public String TOProfitMarkup="";
	public String AffProfitMarkup="";
	
	
	ArrayList<String> RegionArray=new ArrayList<String>();
	
	
	ContentObj content=new ContentObj();
	
	Map<Integer, ItineraryObject> Itinerary=new TreeMap<Integer, ItineraryObject>();
	
	Map<String, assignAir> AirMap=new HashMap<String, assignAir>();
	
	List<HotelDetails> hotellist=new ArrayList<HotelDetails>();
	
	
	List<ActivityDetails> activitylist=new ArrayList<ActivityDetails>();	
	
	
	public List<HotelDetails> getHotellist() {
		return hotellist;
	}

	public void setHotellist(List<HotelDetails> hotellist) {
		this.hotellist = hotellist;
	}

	public void addToAirMap(String origin,assignAir air){
		AirMap.put(origin, air);
		
	}
	public List<ActivityDetails> getActivitylist() {
		return activitylist;
	}

	public void setActivitylist(List<ActivityDetails> activitylist) {
		this.activitylist = activitylist;
	}

	public ArrayList<String> getRegionArray() {
		return RegionArray;
	}

	public void setRegionArray(ArrayList<String> regionArray) {
		RegionArray = regionArray;
	}

	public Map<String, assignAir> getAirMap() {
		return AirMap;
	}

	public void setAirMap(Map<String, assignAir> airMap) {
		AirMap = airMap;
	}

	public Map<Integer, ItineraryObject> getItinerary() {
		return Itinerary;
	}

	public void setItinerary(Map<Integer, ItineraryObject> itinerary) {
		Itinerary = itinerary;
	}

	public ContentObj getContent() {
		return content;
	}

	public void setContent(ContentObj content) {
		this.content = content;
	}

	
	public Map<Integer, ItineraryObject> CreateTheItinerary(String excelPath,int NumberofDays) {
		Map<Integer, ItineraryObject> objmap=new TreeMap<Integer,ItineraryObject>();
		Map<Integer, ItineraryObject> map=new TreeMap<Integer,ItineraryObject>();
		
		ItineraryDetailLoader load=new ItineraryDetailLoader();
		map=load.ItineraryReader(excelPath);
		
		for (int i = 0; i < NumberofDays; i++) {
			objmap.put((i+1), map.get(i+1));
			
		}
		return objmap;
		
	}
	public List<standardinfoObject> readinfo(String excelPath/*,int index*/)throws Exception{
		
		List<standardinfoObject> objlist=new ArrayList<standardinfoObject>();
		ReadExcel ReadExl = new ReadExcel();
	
		ArrayList<Map<Integer, String>> list1 = ReadExl.init(excelPath);
		Map<Integer, String> loginDetails = list1.get(0);
		assignAir air=new assignAir();
		assignHotel hotel=new assignHotel();
		assignActivity activity=new assignActivity();
		int a=0;
		
		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();

		while(it.hasNext())
		{
			String[]   allvalues     = it.next().getValue().split(",");
			standardinfoObject obj = new standardinfoObject();
			 obj=infoSetup(allvalues);
			 obj.setAirMap(air.readinfo(excelPath, obj.packageName));
			 obj.setHotellist(hotel.readinfo(excelPath, obj.packageName));
			 obj.setActivitylist(activity.assignInfo(excelPath, obj.packageName));
			 obj.setItinerary(CreateTheItinerary(excelPath, Integer.parseInt(obj.packageDays)));
			 ContentObjRead ContentRead=new ContentObjRead();
			 obj.setContent(ContentRead.ContentObjReader(excelPath, obj.packageName));
			 
			 objlist.add(a, obj);
			 a++;
			 
		}
		
		
		
		
		 return objlist;
		
	}
	
public Map<String, standardinfoObject> readpackage(String excelPath)throws Exception{
		
		Map<String, standardinfoObject> objMap=new HashMap<String, standardinfoObject>();
		
		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(0);
		
		int a=0;
		
		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();

		while(it.hasNext())
		{
			String[]   allvalues     = it.next().getValue().split(",");
			standardinfoObject obj = new standardinfoObject();
			 obj=infoSetup(allvalues);
			 objMap.put(obj.packageName, obj);
			 a++;
			 
		}
		
		
		
		
		 return objMap;
		
	}

public standardinfoObject getPackage(String PackageName,String excelPath) throws Exception {
	
	standardinfoObject packageSearched=new standardinfoObject();
	Map<String, standardinfoObject> objMap=readpackage(excelPath);
	
	ContentObjRead contObject=new ContentObjRead();
	
	packageSearched=objMap.get(PackageName);
	packageSearched.setContent(contObject.ContentObjReader(excelPath, PackageName));
	
	return packageSearched;
	
}
	
	
	public standardinfoObject infoSetup(String[] valueArray) {
		
		standardinfoObject obj=new standardinfoObject();
		
		obj.url=valueArray[0];
		obj.packageType=valueArray[1];
		obj.packageName=valueArray[2];
		obj.packageDays=valueArray[3];
		obj.ProductIncludes=valueArray[4];
		obj.CostBy=valueArray[5];
		obj.ChildAllowed=valueArray[6];
		obj.StayForm=valueArray[7];
		obj.StayTo=valueArray[8];
		obj.BookingFrom=valueArray[9];
		obj.BookingTO=valueArray[10];
		obj.OriginDefinition=valueArray[11];
		obj.SelectedOriginCountry=valueArray[12];
		obj.SelectedOriginCity=valueArray[13];
		obj.DestinationCountry=valueArray[14];
		obj.DestinationCity=valueArray[15];
		obj.Cancellationrefundtype=valueArray[16];
		obj.CancellationpolicyDateArray=valueArray[17].split("_");
		obj.Cancellationpolicytype=valueArray[18];
		obj.CancellationChargeArray=valueArray[19].split("_");
		obj.NoshowType=valueArray[20];
		obj.Noshowcharge=valueArray[21];
		obj.NoofGroupings=valueArray[22];
		obj.GroupingHeaders=valueArray[23];
		obj.DisplayRanking=valueArray[24];
		obj.DCPMType=valueArray[25];
		obj.DCPM=valueArray[26];
		obj.TOPMType=valueArray[27];
		obj.TOPM=valueArray[28];
		obj.AFFPMType	=valueArray[29];
		obj.AFFPM=valueArray[30];
		obj.TOComType=valueArray[31];
		obj.TOCom=valueArray[32];
		obj.AffComType=valueArray[33];
		obj.AffComType=valueArray[34];
		obj.DepartureBasedOn=valueArray[35];
		obj.SelectedDates=valueArray[36];
		//System.out.println(valueArray[37]);
		
		try {
			ArrayList<String> regionlist=new ArrayList<String>();
			int regionlength=valueArray[37].split("_").length;
						for (int i = 0; i < regionlength; i++) {
				regionlist.add(valueArray[37].split("_")[i]);
				
			}
			
			obj.setRegionArray(regionlist);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
		
		
		return obj;
		
	}
	
	public standardinfoObject returninfo(String[] valueArray) {
		
		standardinfoObject std=new standardinfoObject();
		infoSetup(valueArray);
		return std;
		
	}
	
	public static void main(String[] args) throws Exception {
		standardinfoObject obj1=new standardinfoObject();

		standardinfoObject obj=new standardinfoObject();
		obj1=obj.readinfo("newProdtest.xls").get(0);
System.out.println(obj1.AFFPMType+"KKK");
System.out.println(obj1.TOPMType+"KKKKK");
		/*Map<String, assignAir> airmap=new HashMap<String, assignAir>();
		//airmap=air.readinfo("MultiRegionOriginTest.xls","Multi Region Dates");
		airmap=obj1.getAirMap();
		for (Entry<String, assignAir> entryval:airmap.entrySet()) {
			System.out.println("SSSSSSSSSSSSSSSS");
			System.out.println(entryval.getKey());
			
		}*/
		//System.out.println(obj.readinfo("PackageSetup_prod.xls", 0).get(20).StayForm);
		
		//System.out.println(obj.readinfo("PackageSetup.xls", 0).get(20).StayTo);

		
		
	}
	
	

}
