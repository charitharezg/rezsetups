package setup.com.enumtypes;

public enum HotelInventoryType {
	ALLOTMENTS,FREESELL,ONREQUEST,NONE;
	
	public static HotelInventoryType getInventoryType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("ALLOTMENTS"))return ALLOTMENTS;
		else if(ChargeType.equalsIgnoreCase("FREESELL"))return FREESELL;
		else if(ChargeType.equalsIgnoreCase("ONREQUEST")||ChargeType.equalsIgnoreCase("REQUEST"))return ONREQUEST;
		else return NONE;
		
	}

}
