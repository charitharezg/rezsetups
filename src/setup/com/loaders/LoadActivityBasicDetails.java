package setup.com.loaders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import setup.com.pojo.ActivityRatePlan;
import setup.com.pojo.ActivityType;
import setup.com.pojo.PeriodSetup;
import setup.com.pojo.ProgramType;
import setup.com.pojo.RatePlan;



public class LoadActivityBasicDetails {
	
	Map<Integer, String> ProgramTypeMap 	= null;
	Map<Integer, String> ActivityTypeMap 	= null;
	Map<Integer, String> PeriodSetupMap 	= null;
	Map<Integer, String> RatePlanMap 		= null;
	
	public LoadActivityBasicDetails(ArrayList<Map<Integer, String>> sheetlist) {
		
		ProgramTypeMap 		= sheetlist.get(0);
		ActivityTypeMap 	= sheetlist.get(1);
		PeriodSetupMap 		= sheetlist.get(2);
		RatePlanMap 		= sheetlist.get(3);
		
	}

	//program type
	public ArrayList<ProgramType> loadProgramType(){
		
		Iterator<Map.Entry<Integer, String>> it = ProgramTypeMap.entrySet().iterator();
		ArrayList<ProgramType> ProgramTypeList = new ArrayList<ProgramType>();
		
		while(it.hasNext()) {
			ProgramType programtype = new ProgramType();
			String[] values = it.next().getValue().split(",");
			
			programtype.setActiviy_P_Cat(values[0]);
			programtype.setRank(values[1]);
			programtype.setPick_drop_info(values[2]);
			programtype.setEnable_location(values[3]);
			programtype.setEnable_Act_Type(values[4]);			
			ProgramTypeList.add(programtype);			
		}
	
		return ProgramTypeList;
	}
	
	
	//Activity type
	public ArrayList<ActivityType> loadActivitySetup(){
		
		Iterator<Map.Entry<Integer, String>> it = ActivityTypeMap.entrySet().iterator();
		ArrayList<ActivityType> ActivitySetupList = new ArrayList<ActivityType>();
		
		while(it.hasNext()){
			ActivityType activitysetup = new ActivityType();
			String[] values = it.next().getValue().split(",");
			
			activitysetup.setActivity_type(values[0]);
			activitysetup.setActiviy_P_Cat(values[1]);
			activitysetup.setActivity_desc(values[2]);
			activitysetup.setReturn_Availability(values[3]);
			ActivitySetupList.add(activitysetup);			
		}

		return ActivitySetupList;
	}
	
	
	//Period setup
	public ArrayList<PeriodSetup> loadPeriodSetup(){
		
		Iterator<Map.Entry<Integer, String>> it = PeriodSetupMap.entrySet().iterator();
		ArrayList<PeriodSetup> PeriodSetupList = new ArrayList<PeriodSetup>();
		
		while(it.hasNext()){
			PeriodSetup periodsetup = new PeriodSetup();
			String[] values = it.next().getValue().split(",");
			
			periodsetup.setPeriod_Name(values[0]);
			periodsetup.setCutoff_detup(values[1]);
			periodsetup.setCut_time(values[2]);
			periodsetup.setPeriod_desc(values[3]);
			periodsetup.setDuration_days(values[4]);
			PeriodSetupList.add(periodsetup);
			
		}

		return PeriodSetupList;
	}
	
	
	//RatePlan
	public ArrayList<ActivityRatePlan> loadRatePlans(){
		
		Iterator<Map.Entry<Integer, String>> it = RatePlanMap.entrySet().iterator();
		ArrayList<ActivityRatePlan> RatePlanList = new ArrayList<ActivityRatePlan>();
		
		while(it.hasNext()){
			ActivityRatePlan rates = new ActivityRatePlan();
			String[] values = it.next().getValue().split(",");
			
			rates.setRate_Plan(values[0]);
			rates.setMin_person(values[1]);
			rates.setMax_person(values[2]);
			rates.setRate_calculation(values[3]);
			rates.setRatePlan_desc(values[4]);
			RatePlanList.add(rates);
			
		}

		return RatePlanList;
	}
	
	
}
