package setup.com.packageSetup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import setup.com.dataObjects.standardinfoObject;
import setup.utill.*;


public class AssignProfitMarkupValidations {
	private static WebDriver driver;
	private static Login loginclass;
	private boolean loggedIn=false;
	private int loginAttempts=1;
	private static StringBuffer PrintWriter;

	standardinfoObject obj=new standardinfoObject();
	String Actualstatement;
	String ExpectedStatement;
	String Test;
	
	@BeforeClass
	public static void LOgins() throws Exception{

		loginclass=new Login();
		driver=loginclass.driverinitialize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		PrintWriter           = new StringBuffer();

		loginclass.login(driver);

		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Fixed Package Test Scenario 1-FP_ATS_003</p>");
		PrintWriter.append("<br/>");
		PrintWriter.append("<body>");
		PrintWriter.append("<br><br>");
		PrintWriter.append("<div style='border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;'>");
		PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'><----------Test Description-------><br></p>");
		PrintWriter.append("<p class='InfoSub'>Tested Package   :-</p>");
		PrintWriter.append("<p class='InfoSup'>Test Points</p>");
		PrintWriter.append("<p class='InfoSub'>* Package :-Credit Card Failure</p>");
		PrintWriter.append("<p class='InfoSup'>Search Details</p>");
		PrintWriter.append("<p class='InfoSub'>* Origin :-</p>");
		PrintWriter.append("<p class='InfoSub'>* Destination :-</p>");
		PrintWriter.append("<p class='InfoSub'>* No of Rooms :-</p>");
		PrintWriter.append("<p class='InfoSub'>* Child Included :-</p>");
		PrintWriter.append("<p class='InfoSub'>* Infant Included :-</p>");
		PrintWriter.append(" </div>");

		PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Expected Result</th><th>Actual Result</th><th>Test Status</th></tr>");

	}
	
	@Before
	
	
	public void SetUp()throws Exception{

		/*loginclass=new Login();
		driver=loginclass.driverinitialize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		PrintWriter           = new StringBuffer();

		loginclass.login(driver);*/




	}
	

	@After


	public void testTermianation()throws Exception{


		verifyTrue(ExpectedStatement, Actualstatement, Test);

	}
	@AfterClass

	public static void TearDown()throws Exception{

		PrintWriter.append("</body></html>");
		Reportwriter report=new Reportwriter();
		String location=report.reportwrite();
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(location+"FP_Setup_StandardInfo.html")));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
		// driver.quit();

	}


	public void verifyTrue(String Expected,String Actual,String Message)
	{
		PrintWriter.append("<tr><td>"+"1"+"</td><td>"+Message+"</td>");
		//	TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(Actual.trim().equalsIgnoreCase(Expected.trim()))
				PrintWriter.append("<td class='Passed'>PASS</td>");
			else
				PrintWriter.append("<td class='Failed'>FAIL</td>");

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}
	}

	

}
