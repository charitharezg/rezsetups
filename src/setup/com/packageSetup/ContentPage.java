package setup.com.packageSetup;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import setup.com.dataObjects.standardinfoObject;

public class ContentPage {
	
	public void ContentPageFill(WebDriver driver,standardinfoObject obj) throws AWTException {
		
		
		driver.get(obj.url+"/packaging/setup/PackageContentSetupPage.do");
		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rezgLook0")).click();
		
		driver.switchTo().defaultContent();
	
		((JavascriptExecutor)driver).executeScript("loadEditorVal('shortDescription','Short Description');");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
	//	driver.findElement(By.tagName("body")).clear();
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getShortDescription());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'shortDescription');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
		((JavascriptExecutor)driver).executeScript("loadEditorVal('packageInclusion','Package Inclusion');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getPackageinclusions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'packageInclusion');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
((JavascriptExecutor)driver).executeScript("loadEditorVal('packageExclusion','Package Exclusion');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getPackageExclusions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'packageExclusion');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
((JavascriptExecutor)driver).executeScript("loadEditorVal('packageExclusion','Package Exclusion');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getPackageExclusions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'packageExclusion');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
((JavascriptExecutor)driver).executeScript("loadEditorVal('destinationDetails','Destination Details');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getPackagedescription());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'destinationDetails');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
((JavascriptExecutor)driver).executeScript("loadEditorVal('specialNotes','Special Notes');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getSpecialNotes());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'specialNotes');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
((JavascriptExecutor)driver).executeScript("loadEditorVal('contactDetailsOnArrival','Contact Details on Arrival');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getContacts());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'contactDetailsOnArrival');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
((JavascriptExecutor)driver).executeScript("loadEditorVal('termsAndConditions','Terms and Conditions');");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		
		driver.switchTo().frame("mce_editor_0");
	
		driver.findElement(By.tagName("body")).sendKeys(obj.getContent().getTermsandConditions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'termsAndConditions');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();")	;
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		
		
		
		
		((JavascriptExecutor)driver).executeScript("addImageInfo()");
		
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");
		
		driver.findElement(By.id("thumbnailimage_Y")).click();
		
		
		
		
		
		
		driver.findElement(By.xpath("//*[@id='theFile']")).sendKeys(obj.getContent().getThumbnail());

		driver.findElement(By.id("saveButId")).click();
		
	((JavascriptExecutor)driver).executeScript("close_reload();");
	
	
		
		driver.switchTo().defaultContent();
		
		
		int imagegalleryCount=obj.getContent().getImageGallery().split("#").length;
		
		for (int i = 0; i < imagegalleryCount; i++) {
			
			((JavascriptExecutor)driver).executeScript("addImageInfo()");
			
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");
			
			driver.findElement(By.id("thumbnailimage_N")).click();
			
			
			
			
			
			
			driver.findElement(By.xpath("//*[@id='theFile']")).sendKeys(obj.getContent().getImageGallery().split("#")[i]);
driver.findElement(By.id("caption")).sendKeys("image "+i);
driver.findElement(By.id("displayorder")).clear();
driver.findElement(By.id("displayorder")).sendKeys(String.valueOf(i));

			
			driver.findElement(By.id("saveButId")).click();
			
		((JavascriptExecutor)driver).executeScript("close_reload();");
		
		
			
			driver.switchTo().defaultContent();
			
		}
		((JavascriptExecutor)driver).executeScript("confirmSave();");
		
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");

		
	}
	
	
	

}
