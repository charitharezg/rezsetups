package setup.com.packageSetup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import setup.com.dataObjects.ItineraryObject;
import setup.com.dataObjects.standardinfoObject;
import setup.com.readers.ReadExcel;

public class ItineraryDetailLoader {

	public Map<Integer, ItineraryObject> ItineraryReader(String excelPath) {

		Map<Integer, ItineraryObject> objMap=new TreeMap<Integer, ItineraryObject>();

		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(5);

		int a=0;

		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();

		while(it.hasNext())
		{
			String[]   allvalues     = it.next().getValue().split(",");

			ItineraryObject object=new ItineraryObject();

			object.setDay(allvalues[0]);
			object.setImage(allvalues[1]);
			object.setDescription(allvalues[2]);
			
			objMap.put(Integer.parseInt(allvalues[0]), object);

		}




		return objMap;


	}
	
	
	public void ItineraryDetailsLoader(standardinfoObject object,WebDriver driver) throws InterruptedException {
		
		driver.get(object.url+"/packaging/setup/PackageAssignProductsSetupPage.do");
		
		System.out.println(driver.findElements(By.name("selectedDays")).size());
		
		for (int i = 0; i < driver.findElements(By.name("selectedDays")).size() ; i++) {
			
			ItineraryObject iobject=new ItineraryObject();
			iobject=object.getItinerary().get(i+1);
			
			driver.findElements(By.name("selectedDays")).get(i).click();
			
			Thread.sleep(3000);
			
			driver.findElement(By.id("imageFile")).sendKeys(iobject.getImage());
			driver.findElement(By.id("iternaryDiscription")).sendKeys(iobject.getDescription());
			
			((JavascriptExecutor)driver).executeScript("beforeSave();");
			
			
			
		}
		
	
		
		
		
		
		
	}
	
	public static void main(String[] args) {
		
		ItineraryDetailLoader loader=new ItineraryDetailLoader();
		Map<Integer, ItineraryObject> objMap=new TreeMap<Integer, ItineraryObject>();
		objMap=loader.ItineraryReader("PackageSetup_RezDemo.xls");
		
		System.out.println("size--"+objMap.size());
		
		for (int i = 0; i < objMap.size(); i++) {
			
			System.out.println(objMap.get(i+1).getDay());
			System.out.println(objMap.get(i+1).getImage());
			System.out.println(objMap.get(i+1).getDescription());
		}
		
	}

}
