package setup.com.packageSetup;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.dataObjects.ActivityDetails;
import setup.com.dataObjects.ContentObj;
import setup.com.dataObjects.ContentObjRead;
import setup.com.dataObjects.HotelDetails;
import setup.com.dataObjects.assignActivity;
import setup.com.dataObjects.assignAir;
import setup.com.dataObjects.assignHotel;
import setup.com.dataObjects.costObj;
import setup.com.dataObjects.standardinfoObject;
//import dataObjects.RateObj;
import setup.com.pojo.ResultsLoader;

public class Setup {

	standardinfoObject obj=new standardinfoObject();

	assignHotel hotelObj=new assignHotel();
	assignActivity activityObj=new assignActivity();
	

	public void SatandardInfoPage(WebDriver driver,standardinfoObject obj) throws IOException {

		driver.get(obj.url+"/packaging/setup/PackageSetupPage.do?module=contract");
		
		
		
		//set up page load

		driver.findElement(By.id("screenAction_create")).click();

		
		// package information
		new Select(driver.findElement(By.id("pkg_name_slect"))).selectByVisibleText(obj.packageType);
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("noOfPackageDays")).sendKeys(obj.packageDays);
		driver.findElement(By.id("status_Y")).click();
		driver.findElement(By.id("applicableBookingChannel")).click();
		driver.findElement(By.id("applicableBookingChannelReservation")).click();
		driver.findElement(By.id("displayRanking")).sendKeys(obj.DisplayRanking);

		/*
		 * Product Includes Selection
		 */

		if (obj.ProductIncludes.equalsIgnoreCase("F+H+A")) {

			driver.findElement(By.id("productIncludes_FHA")).click();
		}
		else {
			if (obj.ProductIncludes.equalsIgnoreCase("F+H")) {
				driver.findElement(By.id("productIncludes_FH")).click();
			}
			else {
				if (obj.ProductIncludes.equalsIgnoreCase("F+A")) {
					driver.findElement(By.id("productIncludes_FA")).click();
				}
				else {
					if (obj.ProductIncludes.equalsIgnoreCase("H+A")) {
						driver.findElement(By.id("productIncludes_HA")).click();
					}
					else {
						driver.findElement(By.id("productIncludes_A")).click();
					}
				}
			}
		}

		/*
		 * Costing Type
		 */
		if (obj.CostBy.equalsIgnoreCase("Dynamic")) {
			driver.findElement(By.id("costBy_D")).click();
		}else {
			driver.findElement(By.id("costBy_P")).click();
		}

		/*
		 * Child Allowed
		 */
		if (obj.ChildAllowed.equalsIgnoreCase("Yes")) {
			driver.findElement(By.id("isChildAllowed_Y")).click();
		}else {
			driver.findElement(By.id("isChildAllowed_N")).click();
		}



		// validity period

		String[] day3=obj.StayForm.split("/");

		/*	new Select(driver.findElement(By.id("utilisationPeriodFrom_Day_ID"))).selectByVisibleText(obj.StayForm[0]);
		new Select(driver.findElement(By.id("utilisationPeriodFrom_Month_ID"))).selectByVisibleText(obj.StayForm[1]);
		new Select(driver.findElement(By.id("utilisationPeriodFrom_Year_ID"))).selectByVisibleText(obj.StayForm[2]);
		 */

		String[] day4=obj.StayTo.split("/");
		new Select(driver.findElement(By.id("utilisationPeriodTo_Day_ID"))).selectByVisibleText(day4[0]);
		new Select(driver.findElement(By.id("utilisationPeriodTo_Month_ID"))).selectByVisibleText(day4[1]);
		new Select(driver.findElement(By.id("utilisationPeriodTo_Year_ID"))).selectByVisibleText(day4[2]);


		try {



			if (obj.CostBy.contains("Predefined")) {
				if (obj.DepartureBasedOn.contains("By Dates")) {

					driver.findElement(By.id("departureDateBasedOn_S")).click();
					String dates=obj.SelectedDates.replace("R", ",");
					((JavascriptExecutor)driver).executeScript("$('input[name=departureBookingPeriodDays]').val('"+dates+"');");


				}

			}


		} catch (Exception e) {
			// TODO: handle exception
		}

		/* Booking Period*/

		String[] day2=obj.BookingFrom.split("/");

		/*	new Select(driver.findElement(By.id("bookingPeriodFrom_Day_ID"))).selectByVisibleText(obj.BookingFrom[0]);
		new Select(driver.findElement(By.id("bookingPeriodFrom_Month_ID"))).selectByVisibleText(obj.BookingFrom[1]);
		new Select(driver.findElement(By.id("bookingPeriodFrom_Year_ID"))).selectByVisibleText(obj.BookingFrom[2]);
		 */


		String[] day1=obj.BookingTO.split("/");

		new Select(driver.findElement(By.id("bookingPeriodTo_Day_ID"))).selectByVisibleText(day1[0]);
		new Select(driver.findElement(By.id("bookingPeriodTo_Month_ID"))).selectByVisibleText(day1[1]);
		new Select(driver.findElement(By.id("bookingPeriodTo_Year_ID"))).selectByVisibleText(day1[2]);







		/* Origin Settings*/


		if (obj.OriginDefinition.equalsIgnoreCase("Specific")) {

			driver.findElement(By.id("originDefinition_S")).click();
			driver.findElement(By.id("add_origin_info")).click();

			driver.switchTo().frame("dialogwindow");
			driver.findElement(By.id("countryName")).sendKeys(obj.SelectedOriginCountry);

			driver.findElement(By.id("countryName_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			((JavascriptExecutor)driver).executeScript("selectedRecord(0,'setCountry()');"); 
			driver.switchTo().defaultContent();

			driver.switchTo().frame("dialogwindow");

			driver.findElement(By.id("cityName")).sendKeys(obj.SelectedOriginCity);
			driver.findElement(By.id("cityName_lkup")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("lookup");
			((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');"); 
			driver.switchTo().defaultContent();

			driver.switchTo().frame("dialogwindow");
			((JavascriptExecutor)driver).executeScript("saveData();");
			driver.switchTo().defaultContent();

		}


		// Destination 

		driver.findElement(By.id("add_destination_info")).click();

		driver.switchTo().frame("dialogwindow");
		driver.findElement(By.id("countryName")).sendKeys(obj.DestinationCountry);

		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'setCountry()');"); 
		driver.switchTo().defaultContent();

		driver.switchTo().frame("dialogwindow");

		driver.findElement(By.id("cityName")).sendKeys(obj.DestinationCity);
		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');"); 
		driver.switchTo().defaultContent();

		driver.switchTo().frame("dialogwindow");
		((JavascriptExecutor)driver).executeScript("saveData();");
		driver.switchTo().defaultContent();

		// cancellation Policy

		driver.findElement(By.id("add_can_policy")).click();

		driver.switchTo().frame("dialogwindow");



		/*new Select(driver.findElement(By.id("fromDate_Day_ID"))).selectByVisibleText(obj.StayForm[0]);
		new Select(driver.findElement(By.id("fromDate_Month_ID"))).selectByVisibleText(obj.StayForm[1]);
		new Select(driver.findElement(By.id("fromDate_Year_ID"))).selectByVisibleText(obj.StayForm[2]);
		 */

		new Select(driver.findElement(By.id("toDate_Day_ID"))).selectByVisibleText(day4[0]);
		new Select(driver.findElement(By.id("toDate_Month_ID"))).selectByVisibleText(day4[1]);
		new Select(driver.findElement(By.id("toDate_Year_ID"))).selectByVisibleText(day4[2]);

		if((obj.Cancellationrefundtype).equalsIgnoreCase("Refundable")){

			driver.findElement(By.id("isNonRefundable_N")).click();
			int length=obj.CancellationpolicyDateArray.length;


			for (int i = 0; i < length; i++) {

				driver.findElement(By.id("arrivalDateIsLessThan")).sendKeys(obj.CancellationpolicyDateArray[i]);
				driver.findElement(By.id("standardCancellationBasedOn_R")).click();
				driver.findElement(By.id("resInSc")).sendKeys(obj.CancellationChargeArray[i]);
				driver.findElement(By.id("noShowCancellationBasedOn_R")).click();
				driver.findElement(By.id("resInNc")).sendKeys(obj.Noshowcharge);

			}


		}
		else
		{
			driver.findElement(By.id("isNonRefundable_Y")).click();
		}

		((JavascriptExecutor)driver).executeScript("saveDataCnPlcy();");
		driver.switchTo().defaultContent();


		// hotel grouping
		if (obj.ProductIncludes.contains("H")) {
			String[] groupingarray=obj.GroupingHeaders.split("_");

			int numOfGroupings=Integer.parseInt(obj.NoofGroupings);
			System.out.println(numOfGroupings);
			System.out.println(groupingarray[1]);

			for (int i = 0; i < numOfGroupings; i++) {

				for (int j = Integer.parseInt(groupingarray[i]); j <Integer.parseInt(groupingarray[i+1]); j++) {

					System.out.println(groupingarray[i]);
					System.out.println(j);
					driver.findElement(By.id("daysPending"+j)).click();
				}

				driver.findElement(By.id("addToGroupBtn")).click();	
				System.out.println(i);

			}
		}








		// save

		((JavascriptExecutor)driver).executeScript("beforeSave();");

		/*String message="Data Saved Successfully";
		assertTrue("Successfull Save=Failed", message.equalsIgnoreCase(driver.findElement(By.id("dialogMsgText")).getText()));
		 */

		String packageID=driver.findElement(By.id("packageId")).getAttribute("value");

	//	String Data=obj.packageName+"="+packageID;
//		write.writeFile(Data);
//		snapper.ImageSnapper(driver, "Package creation success", "Standard Info");
		//driver.close();


	}



	public void AssignProducts(WebDriver driver, standardinfoObject obj) throws Exception {


		WebDriverWait  wait = new WebDriverWait(driver, 20);



		driver.get(obj.url+"/packaging/setup/PackageAssignProductsSetupPage.do");



		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		System.out.println(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'loadFixedPackageData()');");

		driver.switchTo().defaultContent();

//		snapper.ImageSnapper(driver, "Product assignment Initial load", "Assign Products");


		// Air assignment

		if ((obj.ProductIncludes.equalsIgnoreCase("F+H+A"))||(obj.ProductIncludes.equalsIgnoreCase("F+H"))||(obj.ProductIncludes.equalsIgnoreCase("F+A"))) {

			/*airobj.readinfo(FliePath, obj.packageName);
			System.out.println("air origin"+airobj.originAir);
			 */
			Map<String, assignAir>airmap=new HashMap<String, assignAir>();
			airmap=obj.getAirMap();

			driver.switchTo().defaultContent();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("airApplicable_Y")));
			Thread.sleep(2000);
			driver.findElement(By.id("airApplicable_Y")).click();

			Thread.sleep(2000);



			for (Entry<String, assignAir> entryval:airmap.entrySet()) {
				assignAir airobj=new assignAir();

				airobj=entryval.getValue();

				((JavascriptExecutor)driver).executeScript("addAirDetails()");

				driver.switchTo().frame("dialogwindow");



				if (obj.CostBy.equalsIgnoreCase("Dynamic")) {

					driver.findElement(By.id("destinationName")).sendKeys(airobj.DestinationAir);
					driver.findElement(By.id("destinationName_lkup")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					((JavascriptExecutor)driver).executeScript("selectedRecord(0,'changeSelect()');");
					driver.switchTo().defaultContent();
					driver.switchTo().frame("dialogwindow");

					driver.findElement(By.id("fareClass")).sendKeys("F");



					//}






				}else  {
					System.out.println(airobj.originAir);
					driver.findElement(By.id("originName")).sendKeys(airobj.originAir);
					driver.findElement(By.id("originName_lkup")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					((JavascriptExecutor)driver).executeScript("selectedRecord(0,'changeSelect()');");
					driver.switchTo().defaultContent();
					driver.switchTo().frame("dialogwindow");

					driver.findElement(By.id("destinationName")).sendKeys(airobj.DestinationAir);
					driver.findElement(By.id("destinationName_lkup")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					((JavascriptExecutor)driver).executeScript("selectedRecord(0,'changeSelect()');");
					driver.switchTo().defaultContent();
					driver.switchTo().frame("dialogwindow");
					System.out.println(airobj.CurrencyCode);
					driver.findElement(By.id("currencyCode")).sendKeys(airobj.CurrencyCode);
					driver.findElement(By.id("currencyCode_lkup")).click();
					driver.switchTo().defaultContent();
					driver.switchTo().frame("lookup");
					((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
					driver.switchTo().defaultContent();
					driver.switchTo().frame("dialogwindow");

					driver.findElement(By.id("adultFare")).clear();
					driver.findElement(By.id("adultFare")).sendKeys(airobj.AdultFare);

					driver.findElement(By.id("adultTotalTax")).clear();
					driver.findElement(By.id("adultTotalTax")).sendKeys(airobj.AdultTax);

					if (obj.ChildAllowed.equalsIgnoreCase("Yes")) {
						driver.findElement(By.id("childFare")).clear();
						driver.findElement(By.id("childFare")).sendKeys(airobj.childFare);

						driver.findElement(By.id("chiledTotalTax")).clear();
						driver.findElement(By.id("chiledTotalTax")).sendKeys(airobj.childTax);

					}



				}

				((JavascriptExecutor)driver).executeScript("saveData();");
				driver.switchTo().defaultContent();



				((JavascriptExecutor)driver).executeScript("beforeSave();");


				try {

					new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("dialogMsgActionButtons"))));
					((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
			/*new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.id("dialogMsgActionButtons")));
			((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
			 */
		}

		//----------------------------------------------------------------------------------------------------------//
		// Hotel Assignment

		List<HotelDetails> hotellist= obj.getHotellist();
		List<WebElement> daylist=driver.findElements(By.xpath(".//*[@id='daysData']/table/tbody/tr/td"));
		if ((obj.ProductIncludes.equalsIgnoreCase("F+H+A"))||(obj.ProductIncludes.equalsIgnoreCase("F+H"))||(obj.ProductIncludes.equalsIgnoreCase("H+A"))) {
			int size=hotellist.size();
			for (int i = 0; i < size; i++) {

				System.out.println("List Size"+daylist.size());

				HotelDetails hotel=hotellist.get(i);


				driver.findElement(By.xpath(".//*[@id='daysData']/table/tbody/tr/td["+hotel.getDay()+"]/input")).click();
				System.out.println(hotel.getDay());

				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotelApplicable_Y")));
				Thread.sleep(2000);
				driver.findElement(By.id("hotelApplicable_Y")).click();
				driver.findElement(By.id("hotelApplicable_Y")).click();
				Thread.sleep(2000);
				new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='hotelGridData']/tbody/tr[1]/td/input")));
				((JavascriptExecutor)driver).executeScript("addHotelDetails()");

				driver.switchTo().frame("dialogwindow");

				driver.findElement(By.id("countryName")).sendKeys(obj.DestinationCountry);
				driver.findElement(By.id("countryName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");
				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				driver.findElement(By.id("cityName")).sendKeys(obj.DestinationCity);
				driver.findElement(By.id("cityName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");
				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				driver.findElement(By.id("hotelName")).clear();
				driver.findElement(By.id("hotelName")).sendKeys(hotel.getHotel());
				driver.findElement(By.id("hotelName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");

				boolean val=true;
				int a=0;

				for (int j = 0; val; j++) {
					System.out.println(hotel.getHotel());
					System.out.println(driver.findElement(By.className("rezgLook"+j)).getText());
					if (driver.findElement(By.className("rezgLook"+j)).getText().contains(hotel.getHotel())) {

						((JavascriptExecutor)driver).executeScript("selectedRecord("+j+",'loadHotelOccupant()');");	
						val=false;


					}	

				}

				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				if (obj.CostBy.equalsIgnoreCase("Dynamic")) {
					System.out.println(driver.findElements(By.xpath("//.//*[@id='roomGridData']/tbody/tr")).size());
					int roomcount=driver.findElements(By.xpath("//.//*[@id='roomGridData']/tbody/tr")).size()-1;

					for (int j = 1; j <= roomcount; j++) {

						String Griddata=driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[1]")).getText().trim();
						String inputdata=hotel.getRoomType().toString().trim();
						System.out.println("Grid"+Griddata);
						System.out.println("In put"+inputdata);
						if (inputdata.contains(Griddata)) {
							System.out.println("if read"+driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[1]")).getText());
							System.out.println("if Load"+hotel.getRoomType());
							driver.findElement(By.id("check_"+(j-1))).click();
						}


					}

				}else {

					System.out.println(driver.findElements(By.xpath(".//*[@id='roomGridData']/tbody/tr")).size());
					int roomcount=driver.findElements(By.xpath(".//*[@id='roomGridData']/tbody/tr")).size()-1;

					for (int j = 1; j <= roomcount; j++) {

						String text1=driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[1]")).getText().trim();
						String text2=hotel.getRoomType().toString().trim();

						if (text2.contains(text1)) {
							System.out.println("if read"+driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[1]")).getText());
							System.out.println("if Load"+hotel.getRoomType());
							driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[7]")).findElement(By.name("single")).click();// Single Room
							driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[8]")).findElement(By.name("double")).click();// Single Room
							driver.findElement(By.xpath(".//*[@id='roomGridData']/tbody/tr["+(j+1)+"]/td[9]")).findElement(By.name("triple")).click();// Single Room


						}


					}

				}
				Thread.sleep(8000);
				((JavascriptExecutor)driver).executeScript("saveHotelData();");
				Thread.sleep(8000);
				driver.switchTo().defaultContent();

				((JavascriptExecutor)driver).executeScript("beforeSave();");
				Thread.sleep(8000);

				new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.id("dialogMsgActionButtons")));
				((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
				Thread.sleep(8000);




			}

		}










		//-----------------activity assignment

		List<ActivityDetails> activitylist= obj.getActivitylist();
		int numOfActivityAssignments=activitylist.size();


		if ((obj.ProductIncludes.equalsIgnoreCase("F+H+A"))||(obj.ProductIncludes.equalsIgnoreCase("F+A"))||(obj.ProductIncludes.equalsIgnoreCase("A"))||(obj.ProductIncludes.equalsIgnoreCase("H+A"))) {


			for (int i = 0; i < numOfActivityAssignments; i++) {
				ActivityDetails activity=activitylist.get(i);

				System.out.println("Day"+activity.getDay());

				driver.findElement(By.xpath(".//*[@id='daysData']/table/tbody/tr/td["+activity.getDay()+"]/input")).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("activityApplicable_Y")));
				Thread.sleep(2000);
				driver.findElement(By.id("activityApplicable_Y")).click();
				driver.findElement(By.id("activityApplicable_Y")).click();

				((JavascriptExecutor)driver).executeScript("addActivityDetails()");

				driver.switchTo().frame("dialogwindow");

				driver.findElement(By.id("countryName")).sendKeys(obj.DestinationCountry);
				driver.findElement(By.id("countryName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");
				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				driver.findElement(By.id("cityName")).sendKeys(obj.DestinationCity);
				driver.findElement(By.id("cityName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");
				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");



				driver.findElement(By.id("displayOrder")).clear();
				driver.findElement(By.id("displayOrder")).sendKeys("3");

				driver.findElement(By.id("programName")).sendKeys(activity.getProgram());
				driver.findElement(By.id("programName_lkup")).click();
				driver.switchTo().defaultContent();

				driver.switchTo().frame("lookup");
				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'loadActivityPeriod()');");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				int activitycount=driver.findElements(By.xpath(".//*[@id='activityGridData']/tbody/tr")).size()-1;
				for (int j = 1; j <= activitycount; j++) {
					String gridActivity=driver.findElement(By.xpath(".//*[@id='activityGridData']/tbody/tr["+(j+1)+"]/td[1]")).getText();
					String inputActivity=activity.getActivityName();

					System.out.println("grid activity"+gridActivity);
					System.out.println("input activity"+inputActivity);

					if (gridActivity.trim().contains(inputActivity.trim()) ) {

						driver.findElement(By.xpath(".//*[@id='activityGridData']/tbody/tr["+(j+1)+"]/td[4]")).findElement(By.name("adult")).click();//adult
						try {
							if (obj.ChildAllowed.equalsIgnoreCase("Yes")) {
								driver.findElement(By.xpath(".//*[@id='activityGridData']/tbody/tr["+(j+1)+"]/td[5]")).findElement(By.name("child")).click();//adult

							}
						} catch (Exception e) {
							// TODO: handle exception
						}

					}





				}

				Thread.sleep(2000);
				((JavascriptExecutor)driver).executeScript("saveData();");
				driver.switchTo().defaultContent();
				((JavascriptExecutor)driver).executeScript("beforeSave();");
				String message1="Data Saved Successfully";
				/*				assertTrue("", message1.equalsIgnoreCase(driver.findElement(By.id("dialogMsgText")).getText()));
				 * 
				 */				new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.id("dialogMsgActionButtons")));
				 ((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");

				 Thread.sleep(4000);

			}
		}


		//*************************Itinerary Details

		ItineraryDetailLoader loads=new ItineraryDetailLoader();
		loads.ItineraryDetailsLoader(obj, driver);






	}


	public void Content(WebDriver driver, standardinfoObject obj) throws InterruptedException {

		ContentObjRead ObjectReader=new ContentObjRead();
		ContentObj object=new ContentObj();
		object=ObjectReader.ContentObjReader("PackageSetup.xls",obj.packageName);

		driver.get(obj.url+"/packaging/setup/PackageContentSetupPage.do");
		driver.findElement(By.id("packageName")).clear();

		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
		driver.switchTo().defaultContent();

		//*******Short Description

		/*	((JavascriptExecutor)driver).executeScript("loadEditorVal('shortDescription','Short Description');");
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		driver.findElement(By.tagName("body")).sendKeys(Keys.BACK_SPACE);
		driver.findElement(By.tagName("body")).sendKeys("\u0008");
		/*driver.findElement(By.tagName("body")).sendKeys(object.getShortDescription());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'shortDescription');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();");
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		 */



		//*******

		/*//***********Package Inclusion
		((JavascriptExecutor)driver).executeScript("loadEditorVal('packageInclusion','Package Inclusion');");
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		driver.findElement(By.tagName("body")).clear();

		driver.findElement(By.tagName("body")).sendKeys(object.getPackageinclusions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'packageInclusion');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();");
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");

		//**************

		//**************Package exclusion
		((JavascriptExecutor)driver).executeScript("loadEditorVal('packageExclusion','Package Exclusion');");
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		driver.findElement(By.tagName("body")).clear();

		driver.findElement(By.tagName("body")).sendKeys(object.getPackageExclusions());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'packageExclusion');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();");
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");

		//***************

		//**************Package Destination
		((JavascriptExecutor)driver).executeScript("loadEditorVal('destinationDetails','Destination Details');");
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		driver.findElement(By.tagName("body")).clear();

		driver.findElement(By.tagName("body")).sendKeys(object.getPackagedescription());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'destinationDetails');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();");
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");


				//***************

		//**************Package Special Notes
		((JavascriptExecutor)driver).executeScript("loadEditorVal('specialNotes','Special Notes');");
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		driver.findElement(By.tagName("body")).clear();

		driver.findElement(By.tagName("body")).sendKeys(object.getSpecialNotes());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		((JavascriptExecutor)driver).executeScript("parent.submitSubValue(getEditorValue(),'specialNotes');");
		driver.switchTo().defaultContent();
		((JavascriptExecutor)driver).executeScript("submitEditorValues();");
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		 */
		//***************
		//**************Package Thumbnail

		/*driver.findElement(By.className("selbut1")).click();
		driver.switchTo().frame("dialogwindow");
		 */	//driver.switchTo().frame("dialogwindowiframe");
		//driver.findElement(By.id("thumbnailimage_Y")).click();
		//driver.findElement(By.id("theFile")).sendKeys("Thumbnail_Image.jpg");
		//driver.findElement(By.id("saveButId")).click();


		//***************

		//**************

		File folder = new File("Images");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {

				driver.switchTo().defaultContent();
				driver.findElement(By.className("selbut1")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");

				if (listOfFiles[i].getName().contains("Thumbnail_Image.jpg")) {
					//driver.findElement(By.id("theFile")).clear();
					driver.findElement(By.id("thumbnailimage_Y")).click();
					driver.findElement(By.id("theFile")).sendKeys(listOfFiles[i].getName());
					driver.findElement(By.id("saveButId")).click();
				}else {
					System.out.println("Directory " + listOfFiles[i].getName());
					driver.switchTo().defaultContent();
					driver.switchTo().frame("dialogwindow");
					driver.findElement(By.id("theFile")).sendKeys(listOfFiles[i].getName());
					driver.findElement(By.id("saveButId")).click();
				}
			} else  {

				/* System.out.println("Directory " + listOfFiles[i].getName());
		        driver.findElement(By.id("theFile")).sendKeys(listOfFiles[i].getName());*/
			}
		}
		//**************


		/* for (int i = 1; i < 6; i++) {
		    	((JavascriptExecutor) driver) .executeScript("($('.standalonelink')).click()");
		    	driver.switchTo().frame("dialogwindow");
		    	try { 
		    		if (i == 1) 
		    			driver.findElement(By.id("thumbnailimage_Y")).click();
		    		else 
		    			driver.findElement(By.id("caption")).sendKeys(" "+i); 
		    		} catch (Exception e) { // TODO: handle exception 
		    	} 
		    	String FullPath = new File(CurrentHotel.getPath()).getAbsolutePath(); driver.findElement(By.id("theFile")).sendKeys( FullPath + System.getProperty("file.separator") + i + ".jpg"); 
		    	driver.findElement(By.id("saveButId")).click(); 
		    	((JavascriptExecutor) driver) .executeScript("javascript:close_reload();"); 
		    	driver.switchTo().defaultContent();
		    	}*/




		//driver.findElement(By.id("saveButId")).click();
		((JavascriptExecutor) driver).executeScript("javascript:close_reload();");
		driver.switchTo().defaultContent();


	}
	public void AssignPM(WebDriver driver,standardinfoObject obj) {

		driver.get(obj.url+"/packaging/setup/PackageMarkupCommissionSetupPage.do");	
		//driver.findElement(By.id("AssignMarkup&Commission_lkinid")).click();
		driver.findElement(By.id("packageName")).clear();

		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
		driver.switchTo().defaultContent();

		if (obj.CostBy.equalsIgnoreCase("Dynamic")) {


			// DC PM assignment
			driver.findElement(By.id("partnerType_DC")).click();
			if (obj.DCPMType.equalsIgnoreCase("Percentage")) {
				driver.findElement(By.id("markupByDC_P")).click();
			}else {

			}


			driver.findElement(By.id("markupAmountDC")).clear();
			driver.findElement(By.id("markupAmountDC")).sendKeys(obj.DCPM);


			//TO(Net rate) PM assignment
			driver.findElement(By.id("partnerType_TON")).click();

			if (obj.TOPMType.equalsIgnoreCase("Percentage")) {
				driver.findElement(By.id("markupByTON_P")).click();
			}else {
				driver.findElement(By.id("markupByTON_V")).click();
			}



			driver.findElement(By.id("markupAmountTON")).clear();
			driver.findElement(By.id("markupAmountTON")).sendKeys(obj.TOPM);

			driver.findElement(By.xpath(".//*[@id='tourOperatorWiseTONBlockId']/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/input")).click();

			/*((JavascriptExecutor)driver).executeScript("addToList()");*/


			driver.findElement(By.id("partnerType_TOC")).click();

			if (obj.TOPMType.equalsIgnoreCase("Percentage")) {
				driver.findElements(By.id("markupByTOC_P")).get(0).click();
			}else {
				driver.findElements(By.id("markupByTOC_P")).get(1).click();

			}


			driver.findElement(By.id("markupAmountTOC")).clear();
			driver.findElement(By.id("markupAmountTOC")).sendKeys(obj.TOPM);
			if (obj.TOComType.contains("Percentage")) {
				driver.findElement(By.id("commissionTypeTOC_P")).click();
			}else {
				driver.findElement(By.id("commissionTypeTOC_V")).click();
			}

			driver.findElement(By.id("commisionAmountTOC")).clear();
			driver.findElement(By.id("commisionAmountTOC")).sendKeys(obj.TOCom);
			driver.findElement(By.xpath(".//*[@id='tourOperatorWiseTOCBlockId']/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/input")).click();

			/*((JavascriptExecutor)driver).executeScript("addToList()");*/

		}else  {

			driver.findElement(By.id("markupByPRE_P")).click();
			driver.findElement(By.id("markupAmountPRE")).clear();
			driver.findElement(By.id("markupAmountPRE")).sendKeys(obj.DCPM);

			driver.findElement(By.id("commisionAmountPRE")).clear();
			driver.findElement(By.id("commisionAmountPRE")).sendKeys(obj.TOPM);

			driver.findElement(By.xpath(".//*[@id='tourOperatorWisePREBlockId']/table[1]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/input")).click();
			//((JavascriptExecutor)driver).executeScript("addToList()");

			driver.findElement(By.id("commisionAmountPRE_AFF")).clear();
			driver.findElement(By.id("commisionAmountPRE_AFF")).sendKeys(obj.AFFPM);
			driver.findElement(By.xpath(".//*[@id='tourOperatorWisePREBlockId']/table[2]/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/input")).click();

			//	((JavascriptExecutor)driver).executeScript("addAffList()");


			((JavascriptExecutor)driver).executeScript("confirmSave('submit');");
		}





		((JavascriptExecutor)driver).executeScript("confirmSave('submit');");


	}

	public StringBuffer costing_New(WebDriver driver, standardinfoObject obj) throws InterruptedException, ParseException, IOException {
		
		StringBuffer PrintWriter=new StringBuffer();
		Map<String, Map<String, Map<String, Map<String, costObj>>>> validationmap=new HashMap<String, Map<String,Map<String,Map<String,costObj>>>>();
		CostingRegionwise costing=new CostingRegionwise();
		validationmap=costing.TotalCostingObjectConstruct(obj);
		ResultsLoader loader=new ResultsLoader();

		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">Pre Defined Costing Restuls </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Fixed Packages-Pre defined Costing</p>");
		PrintWriter.append("<br/>");
		PrintWriter.append("<body>");
		PrintWriter.append("<br><br>");


		PrintWriter.append("<div style='border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;'>");
		PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'><----------Test Description-------><br></p>");
		PrintWriter.append("<p class='InfoSub'>Tested Package   :-"+obj.packageName+"</p>");
		PrintWriter.append("<p class='InfoSup'>Test Points</p>");
		PrintWriter.append("<p class='InfoSub'>* REZPCK_6766-Tax calculation and its effects for the other costings Generating</p>");

		PrintWriter.append(" </div>");

		driver.get(obj.url+"/packaging/setup/PackageCostingSetupPage.do");


		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");

		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'packageNameChange()');");
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		
		
		driver.findElement(By.id("origin_lkup"));

		int numberofRegions=obj.getRegionArray().size();
		System.out.println("numberofRegions"+numberofRegions);
		Map<String, assignAir> originmap=new HashMap<String, assignAir>();
		
		if (obj.ProductIncludes.contains("F")) {
			originmap=obj.getAirMap();
		}else{
			assignAir landorigin=new assignAir();
			landorigin.setOriginAir("Land Only");
			originmap.put("Land Only", landorigin);
			
		}


		for (Entry<String, assignAir> entryVal:originmap.entrySet()) {

			for (int i = 0; i < numberofRegions; i++) {

				driver.findElement(By.id("regionName")).clear();
				driver.findElement(By.id("regionName")).sendKeys(obj.getRegionArray().get(i));
				driver.findElement(By.id("regionName_lkup")).click();
				driver.switchTo().frame("lookup");

				((JavascriptExecutor)driver).executeScript("selectedRecord(0,'clearValues()');");
				driver.switchTo().defaultContent();
				
				
				

				if (obj.ProductIncludes.contains("F")) {
					
					try {
						driver.findElement(By.id("origin")).clear();
						driver.findElement(By.id("origin")).sendKeys(entryVal.getValue().getOriginAir());
						driver.findElement(By.id("origin_lkup")).click();
						driver.switchTo().frame("lookup");

						int originscount=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).size();
						boolean airloop=true;
						for (int j = 0;  airloop; j++) {
							
							String airport=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(j).findElement(By.tagName("td")).getText();
							System.out.println(airport+"air");
							if (airport.contains(entryVal.getValue().getOriginAir())) {
								
								((JavascriptExecutor)driver).executeScript("selectedRecord("+j+",'clearValues()');");
								airloop=false;
								
								
							}
						}
						
						driver.switchTo().defaultContent();	
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				
driver.findElement(By.id("packageCurrencyCode")).clear();
driver.findElement(By.id("packageCurrencyCode")).sendKeys("OMR");
driver.findElement(By.id("packageCurrencyCode_lkup")).click();
driver.switchTo().frame("lookup");

int currencycount=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).size();
boolean currencyloop=true;
for (int j = 0;  currencyloop; j++) {
	
	String currency=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(j).findElement(By.tagName("td")).getText();
	if (currency.contains("OMR")) {
		
		((JavascriptExecutor)driver).executeScript("selectedRecord("+j+",'clearValues()');");
		currencyloop=false;
		
		
	}
}

driver.switchTo().defaultContent();	

				
				 
				((JavascriptExecutor)driver).executeScript("getCostStatus()");

				if (obj.DepartureBasedOn.contains("By Dates")) {


					int numberofDays=obj.SelectedDates.split("R").length;


					List<Map<String, String>> savingMessageArrayStrings=new ArrayList<Map<String,String>>();

					List<Map<String, String>> savingMessageArrayStringsV=new ArrayList<Map<String,String>>();


System.out.println("numberofDays"+numberofDays);

					for (int i1 = 0; i1 < numberofDays; i1++) {

						Map<String, String> ExpectedM=new HashMap<String, String>();
						Map<String, String> ActualM=new HashMap<String, String>();

						String CurrentDate=obj.SelectedDates.split("R")[i1];
						String testmessage="Costing is performed successfully for "+CurrentDate;
						String OverallTestingV=null;
						if (i1==(numberofDays-1)) {
							OverallTestingV="Package is already costed and ready for sale";
						}else {
							OverallTestingV="Package is not yet costed for sale.";
						}

						ExpectedM.put("Alert", testmessage);
						ExpectedM.put("Overall", OverallTestingV);

						Map<String, costObj> RateMapGenerated=new HashMap<String, costObj>();
						Map<String, costObj> RateMapSaved=new HashMap<String, costObj>();
						
						Thread.sleep(4000);
						driver.findElement(By.id("packageCurrencyCode"));

						((JavascriptExecutor)driver).executeScript("generatePackageCost('"+obj.SelectedDates.split("R")[i1]+"','','')");

						RateMapGenerated=ReadRates(driver);

						((JavascriptExecutor)driver).executeScript(" saveCosting();");
						Thread.sleep(4000);
						String alertmsg=driver.findElement(By.id("dialogMsgText")).getText();

						((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox)");

						String Overallmsg=driver.findElement(By.id("costingStatus")).getAttribute("value");

						ActualM.put("Alert", alertmsg);
						ActualM.put("Overall", Overallmsg);
						Map<String , Map<String, costObj>>Ratemaps=new HashMap<String, Map<String,costObj>>();


						((JavascriptExecutor)driver).executeScript("viewCostedData('"+obj.SelectedDates.split("R")[i1]+"','','')");
						RateMapSaved=ReadRates(driver);
						Map<String, costObj>Expected=validationmap.get(entryVal.getValue().getOriginAir()).get(obj.getRegionArray().get(i)).get(obj.SelectedDates.split("R")[i1]);
						Ratemaps.put("Generated", RateMapGenerated);
						Ratemaps.put("Saved", RateMapSaved);
						Ratemaps.put("Expected", Expected);

						//ResultsLoader loader=new ResultsLoader();
						PrintWriter.append("<p class='InfoSup'>Origin:"+ entryVal.getKey()+"</p>");
						PrintWriter.append("<p class='InfoSup'>Region:"+ obj.getRegionArray().get(i) +"</p>");
						PrintWriter.append("<p class='InfoSup'>Date:"+ obj.SelectedDates.split("R")[i1]+"</p>");

						PrintWriter.append(loader.resultsGenerator(Ratemaps,obj.SelectedDates.split("R")[i1]));

						savingMessageArrayStringsV.add(ExpectedM);
						savingMessageArrayStrings.add(ActualM);





					}

				}else {
					
					Map<String , Map<String, costObj>>Ratemaps=new HashMap<String, Map<String,costObj>>();

					List<Map<String, String>> savingMessageArrayStrings=new ArrayList<Map<String,String>>();

					List<Map<String, String>> savingMessageArrayStringsV=new ArrayList<Map<String,String>>();

					SimpleDateFormat sdf1=new SimpleDateFormat("dd/MMM/yyyy");
					Date dateFrom=sdf1.parse(obj.StayForm);
					Date dateTo=sdf1.parse(obj.StayTo);

					try {



						((JavascriptExecutor)driver).executeScript("viewPeriod('','"+new SimpleDateFormat("yyyy-MM-dd").format(dateFrom)+"','"+new SimpleDateFormat("yyyy-MM-dd").format(dateTo)+"')");

					} catch (Exception e) {
						// TODO: handle exception
					}
					int numberofPeriods=obj.SelectedDates.split("R").length;


					for (int a = 0; a < numberofPeriods; a++) {

						Map<String, String> ExpectedM=new HashMap<String, String>();
						Map<String, String> ActualM=new HashMap<String, String>();

						String DateFromCurrent = null;
						String OverallTestingV=null;
						if (a==0) {

							DateFromCurrent=new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MMM/yyyy").parse(obj.StayForm));
						}else {
							DateFromCurrent=DateCalculator(obj.SelectedDates.split("R")[a-1], 1);
						}
						if (a==(numberofPeriods-1)) {
							OverallTestingV="Package is already costed and ready for sale.";
						}else {
							OverallTestingV="Package is not yet costed for sale.";
						}

						String DateToCurrent=obj.SelectedDates.split("R")[a];
						System.out.println(DateToCurrent);


						String testmessage="Costing is performed successfully for "+GettheDate(DateFromCurrent, "m")+"-"+GettheDate(DateFromCurrent, "d")+"-"+GettheDate(DateFromCurrent, "y")+" - "+GettheDate(DateToCurrent, "m")+"-"+GettheDate(DateToCurrent, "d")+"-"+GettheDate(DateToCurrent, "y");
						ExpectedM.put("Alert", testmessage);
						ExpectedM.put("Overall", OverallTestingV);
						System.out.println(testmessage);
						try {


							new Select(driver.findElement(By.id("toDate_Day_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[a], "d"));
							new Select(driver.findElement(By.id("toDate_Month_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[a], "m"));
							new Select(driver.findElement(By.id("toDate_Year_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[a], "y"));
						} catch (Exception e) {
							// TODO: handle exception
						}

						((JavascriptExecutor)driver).executeScript("validateDate()");

						Map<String, costObj> RateMapGenerated=new HashMap<String, costObj>();
						Map<String, costObj> RateMapSaved=new HashMap<String, costObj>();
						RateMapGenerated=ReadRates(driver);

						((JavascriptExecutor)driver).executeScript("saveCosting();");
						RateMapSaved=ReadRates(driver);

						String alertmsg=driver.findElement(By.id("dialogMsgText")).getText();

						((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox)");

						String Overallmsg=driver.findElement(By.id("costingStatus")).getAttribute("value");

						ActualM.put("Alert", alertmsg);
						ActualM.put("Overall", Overallmsg);

						String fromDate=DateCalculator(obj.SelectedDates.split("R")[a], 1);


						//CostingValidations validator=new CostingValidations();
						Map<String, costObj>Expected=validationmap.get(entryVal.getValue().getOriginAir()).get(obj.getRegionArray().get(i)).get(obj.SelectedDates.split("R")[a]);
						Ratemaps.put("Generated", RateMapGenerated);

						Ratemaps.put("Expected", Expected);
						Ratemaps.put("Saved", RateMapSaved)	;

						PrintWriter.append(loader.resultsGenerator(Ratemaps,obj.SelectedDates.split("R")[a]));

						//PrintWriter.append(loader.AlertTextLoader(savingMessageArrayStringsV, savingMessageArrayStrings));
						try {
							((JavascriptExecutor)driver).executeScript("viewPeriod('','"+fromDate+"','"+obj.SelectedDates.split("R")[a+1]+"')");

						} catch (Exception e) {
							// TODO: handle exception
						}

						savingMessageArrayStringsV.add(ExpectedM);
						savingMessageArrayStrings.add(ActualM);

					}
					PrintWriter.append(loader.AlertTextLoader(savingMessageArrayStringsV, savingMessageArrayStrings));

				}






			}





		}
		return PrintWriter;


	}
	
	
	public void costingFlow(WebDriver driver,standardinfoObject obj,String Air) {
		
		
		int numberofRegions=obj.getRegionArray().size();

		for (int i = 0; i < numberofRegions; i++) {

			driver.findElement(By.id("regionName")).clear();
			driver.findElement(By.id("regionName")).sendKeys(obj.getRegionArray().get(i));
			driver.findElement(By.id("regionName_lkup")).click();
			driver.switchTo().frame("lookup");

			((JavascriptExecutor)driver).executeScript("selectedRecord(0,'clearValues()');");
			driver.switchTo().defaultContent();

			if (obj.ProductIncludes.contains("F")) {
				
				try {
					driver.findElement(By.id("origin")).clear();
					driver.findElement(By.id("origin")).sendKeys(Air);
					driver.findElement(By.id("origin_lkup")).click();
					driver.switchTo().frame("lookup");

					int originscount=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).size();
					boolean airloop=true;
					for (int j = 0;  airloop; j++) {
						
						String airport=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(j).findElement(By.tagName("td")).getText();
						System.out.println(airport+"air");
						if (airport.contains(Air)) {
							
							((JavascriptExecutor)driver).executeScript("selectedRecord("+j+",'clearValues()');");
							airloop=false;
							
							
						}
					}
					
					driver.switchTo().defaultContent();	
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}

			((JavascriptExecutor)driver).executeScript("getCostStatus()");

			

		}





	
		
	}
	public StringBuffer Costing(WebDriver driver,standardinfoObject obj,String filePath) throws Exception {
		return null;/*

		StringBuffer PrintWriter=new StringBuffer();
		ResultsLoader loader=new ResultsLoader();

		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">Pre Defined Costing Restuls </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Fixed Packages-Pre defined Costing</p>");
		PrintWriter.append("<br/>");
		PrintWriter.append("<body>");
		PrintWriter.append("<br><br>");


		PrintWriter.append("<div style='border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;'>");
		PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'><----------Test Description-------><br></p>");
		PrintWriter.append("<p class='InfoSub'>Tested Package   :-"+obj.packageName+"</p>");
		PrintWriter.append("<p class='InfoSup'>Test Points</p>");
		PrintWriter.append("<p class='InfoSub'>* REZPCK_6766-Tax calculation and its effects for the other costings Generating</p>");

		PrintWriter.append(" </div>");


		driver.get(obj.url+"/packaging/setup/PackageCostingSetupPage.do");


		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");

		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");
		driver.switchTo().defaultContent();


		driver.findElement(By.id("regionName")).sendKeys("All");
		driver.findElement(By.id("regionName_lkup")).click();
		driver.switchTo().frame("lookup");

		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'clearValues()');");
		driver.switchTo().defaultContent();

		if ((obj.ProductIncludes.equalsIgnoreCase("F+H+A"))||(obj.ProductIncludes.equalsIgnoreCase("F+A"))||(obj.ProductIncludes.equalsIgnoreCase("F+H"))) {

			driver.findElement(By.id("origin")).sendKeys(obj.SelectedOriginCity);
			driver.findElement(By.id("origin_lkup")).click();
			driver.switchTo().frame("lookup");

			((JavascriptExecutor)driver).executeScript("selectedRecord(0,'clearValues()');");
			driver.switchTo().defaultContent();	
		}

		driver.findElement(By.id("packageCurrencyCode")).clear();
		driver.findElement(By.id("packageCurrencyCode")).sendKeys("USD");

		driver.findElement(By.id("packageCurrencyCode_lkup")).click();
		driver.switchTo().frame("lookup");

		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'clearValues()');");
		driver.switchTo().defaultContent();	



		if (obj.DepartureBasedOn.contains("By Dates")) {

			int numberofDays=obj.SelectedDates.split("R").length;


			List<Map<String, String>> savingMessageArrayStrings=new ArrayList<Map<String,String>>();

			List<Map<String, String>> savingMessageArrayStringsV=new ArrayList<Map<String,String>>();



			for (int i = 0; i < numberofDays; i++) {

				Map<String, String> ExpectedM=new HashMap<String, String>();
				Map<String, String> ActualM=new HashMap<String, String>();

				String CurrentDate=obj.SelectedDates.split("R")[i];
				String testmessage="Costing is performed successfully for "+CurrentDate;
				String OverallTestingV=null;
				if (i==(numberofDays-1)) {
					OverallTestingV="Package is already costed and ready for sale";
				}else {
					OverallTestingV="Package is not yet costed for sale.";
				}

				ExpectedM.put("Alert", testmessage);
				ExpectedM.put("Overall", OverallTestingV);
				Map<String , Map<String, RateObj>>Ratemaps=new HashMap<String, Map<String,RateObj>>();


				Map<String, RateObj> RateMapGenerated=new HashMap<String, RateObj>();
				Map<String, RateObj> RateMapSaved=new HashMap<String, RateObj>();

				((JavascriptExecutor)driver).executeScript("generatePackageCost('"+obj.SelectedDates.split("R")[i]+"','','')");

				RateMapGenerated=ReadRates(driver);

				((JavascriptExecutor)driver).executeScript(" saveCosting();");
				Thread.sleep(4000);
				String alertmsg=driver.findElement(By.id("dialogMsgText")).getText();

				((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox)");

				String Overallmsg=driver.findElement(By.id("costingStatus")).getAttribute("value");

				ActualM.put("Alert", alertmsg);
				ActualM.put("Overall", Overallmsg);


				((JavascriptExecutor)driver).executeScript("viewCostedData('"+obj.SelectedDates.split("R")[i]+"','','')");
				RateMapSaved=ReadRates(driver);
				CostingValidations validator=new CostingValidations();
				Map<String, RateObj>Expected=validator.RateMapGenerator(obj.packageName, filePath);
				Ratemaps.put("Generated", RateMapGenerated);
				Ratemaps.put("Saved", RateMapSaved);
				Ratemaps.put("Expected", Expected);

				//ResultsLoader loader=new ResultsLoader();
				PrintWriter.append(loader.resultsGenerator(Ratemaps,obj.SelectedDates.split("R")[i]));

				savingMessageArrayStringsV.add(ExpectedM);
				savingMessageArrayStrings.add(ActualM);


			}

			PrintWriter.append(loader.AlertTextLoader(savingMessageArrayStringsV, savingMessageArrayStrings));


		}else {

			Map<String , Map<String, RateObj>>Ratemaps=new HashMap<String, Map<String,RateObj>>();

			List<Map<String, String>> savingMessageArrayStrings=new ArrayList<Map<String,String>>();

			List<Map<String, String>> savingMessageArrayStringsV=new ArrayList<Map<String,String>>();

			SimpleDateFormat sdf1=new SimpleDateFormat("dd/MMM/yyyy");
			Date dateFrom=sdf1.parse(obj.StayForm);
			Date dateTo=sdf1.parse(obj.StayTo);

			try {



				((JavascriptExecutor)driver).executeScript("viewPeriod('','"+new SimpleDateFormat("yyyy-MM-dd").format(dateFrom)+"','"+new SimpleDateFormat("yyyy-MM-dd").format(dateTo)+"')");

			} catch (Exception e) {
				// TODO: handle exception
			}
			int numberofPeriods=obj.SelectedDates.split("R").length;


			for (int i = 0; i < numberofPeriods; i++) {

				Map<String, String> ExpectedM=new HashMap<String, String>();
				Map<String, String> ActualM=new HashMap<String, String>();

				String DateFromCurrent = null;
				String OverallTestingV=null;
				if (i==0) {

					DateFromCurrent=new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MMM/yyyy").parse(obj.StayForm));
				}else {
					DateFromCurrent=DateCalculator(obj.SelectedDates.split("R")[i-1], 1);
				}
				if (i==(numberofPeriods-1)) {
					OverallTestingV="Package is already costed and ready for sale.";
				}else {
					OverallTestingV="Package is not yet costed for sale.";
				}

				String DateToCurrent=obj.SelectedDates.split("R")[i];
				System.out.println(DateToCurrent);


				String testmessage="Costing is performed successfully for "+GettheDate(DateFromCurrent, "m")+"-"+GettheDate(DateFromCurrent, "d")+"-"+GettheDate(DateFromCurrent, "y")+" - "+GettheDate(DateToCurrent, "m")+"-"+GettheDate(DateToCurrent, "d")+"-"+GettheDate(DateToCurrent, "y");
				ExpectedM.put("Alert", testmessage);
				ExpectedM.put("Overall", OverallTestingV);
				System.out.println(testmessage);
				try {


					new Select(driver.findElement(By.id("toDate_Day_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[i], "d"));
					new Select(driver.findElement(By.id("toDate_Month_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[i], "m"));
					new Select(driver.findElement(By.id("toDate_Year_ID"))).selectByVisibleText(GettheDate(obj.SelectedDates.split("R")[i], "y"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				((JavascriptExecutor)driver).executeScript("validateDate()");

				Map<String, RateObj> RateMapGenerated=new HashMap<String, RateObj>();
				Map<String, RateObj> RateMapSaved=new HashMap<String, RateObj>();
				RateMapGenerated=ReadRates(driver);

				((JavascriptExecutor)driver).executeScript("saveCosting();");
				RateMapSaved=ReadRates(driver);

				String alertmsg=driver.findElement(By.id("dialogMsgText")).getText();

				((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox)");

				String Overallmsg=driver.findElement(By.id("costingStatus")).getAttribute("value");

				ActualM.put("Alert", alertmsg);
				ActualM.put("Overall", Overallmsg);

				String fromDate=DateCalculator(obj.SelectedDates.split("R")[i], 1);


				CostingValidations validator=new CostingValidations();
				Map<String, RateObj>Expected=validator.RateMapGenerator(obj.packageName, filePath);
				Ratemaps.put("Generated", RateMapGenerated);

				Ratemaps.put("Expected", Expected);
				Ratemaps.put("Saved", RateMapSaved)	;

				PrintWriter.append(loader.resultsGenerator(Ratemaps,obj.SelectedDates.split("R")[i]));

				//PrintWriter.append(loader.AlertTextLoader(savingMessageArrayStringsV, savingMessageArrayStrings));
				try {
					((JavascriptExecutor)driver).executeScript("viewPeriod('','"+fromDate+"','"+obj.SelectedDates.split("R")[i+1]+"')");

				} catch (Exception e) {
					// TODO: handle exception
				}

				savingMessageArrayStringsV.add(ExpectedM);
				savingMessageArrayStrings.add(ActualM);

			}
			PrintWriter.append(loader.AlertTextLoader(savingMessageArrayStringsV, savingMessageArrayStrings));

		}


		//((JavascriptExecutor)driver).executeScript("generatePackageCost()");

		Thread.sleep(15000);
		driver.switchTo().defaultContent();	

		String[] type={"Single","Double","Triple","Child"};



		Map<String, Map<String, Integer>>ActualMap=new HashMap<String, Map<String,Integer>>();
		Map<String, Map<String, Integer>>ExpectedMap=new HashMap<String, Map<String,Integer>>();

		Map<String, RateObj> RateMapGenerated=new HashMap<String, RateObj>();
		Map<String, RateObj> RateMapSaved=new HashMap<String, RateObj>();


		for (int i = 0; i < type.length; i++) {

			RateObj rates=new RateObj();
			String occType=type[i];
			System.out.println(occType);

			try {
				rates.setAir(driver.findElements(By.id("air"+occType+"Value")).get(1).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				rates.setHotel(driver.findElements(By.id("hotel"+occType+"Value")).get(1).getText().trim());			

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				rates.setProgram(driver.findElements(By.id("program"+occType+"Value")).get(1).getText().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}

			rates.setTotalCost(driver.findElements(By.id("totalCost"+occType)).get(1).getText().trim());
			rates.setAdjustableTotalCost(driver.findElement(By.id("txtAdjustTotalCost"+occType)).getAttribute("value").trim());

			rates.setTotalTaxValue(driver.findElements(By.id("totalTax"+occType)).get(1).getText().trim());

			rates.setProfitMarkupAmount(driver.findElement(By.id("lblProfitMarkup"+occType)).getText().trim());
			rates.setProposedSellRate(driver.findElement(By.id("lblProposedSellRate"+occType)).getText().trim());
			rates.setFinalSellRate(driver.findElement(By.id("txtFinalSellRate"+occType)).getAttribute("value").trim());
			rates.setAgentCommissionableAmount(driver.findElement(By.id("lblAgentCommissionable"+occType)).getText().trim());
			rates.setAgentCommissionAmount(driver.findElement(By.id("lblAgentCommission"+occType)).getText().trim());
			rates.setAgentNetProfit(driver.findElement(By.id("lblNetProfit"+occType)).getText().trim());
			rates.setAffiliateCommissionableAmount(driver.findElement(By.id("lblAffiliateCommissionable"+occType)).getText().trim());
			rates.setAffiliateCommissionAmount(driver.findElement(By.id("lblAffiliateCommission"+occType)).getText().trim());
			rates.setAffiliateNetProfit(driver.findElement(By.id("lblAffiliateNetProfit"+occType)).getText().trim());


			RateMapGenerated.put(occType, rates);



		}

		((JavascriptExecutor)driver).executeScript(" saveCosting();");
		Thread.sleep(4000);


		((JavascriptExecutor)driver).executeScript("getCostStatus()");

		Thread.sleep(4000);

		for (int i = 0; i < type.length; i++) {

			RateObj rates=new RateObj();
			String occType=type[i];


			try {
				rates.setAir(driver.findElements(By.id("air"+occType+"Value")).get(1).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				rates.setHotel(driver.findElements(By.id("hotel"+occType+"Value")).get(1).getText().trim());			

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {

				rates.setProgram(driver.findElements(By.id("program"+occType+"Value")).get(1).getText().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}

			rates.setTotalCost(driver.findElements(By.id("totalCost"+occType)).get(1).getText().trim());
			rates.setAdjustableTotalCost(driver.findElement(By.id("txtAdjustTotalCost"+occType)).getAttribute("value").trim());

			rates.setTotalTaxValue(driver.findElements(By.id("totalTax"+occType)).get(1).getText().trim());

			rates.setProfitMarkupAmount(driver.findElement(By.id("lblProfitMarkup"+occType)).getText().trim());
			rates.setProposedSellRate(driver.findElement(By.id("lblProposedSellRate"+occType)).getText().trim());
			rates.setFinalSellRate(driver.findElement(By.id("txtFinalSellRate"+occType)).getAttribute("value").trim());
			rates.setAgentCommissionableAmount(driver.findElement(By.id("lblAgentCommissionable"+occType)).getText().trim());
			rates.setAgentCommissionAmount(driver.findElement(By.id("lblAgentCommission"+occType)).getText().trim());
			rates.setAgentNetProfit(driver.findElement(By.id("lblNetProfit"+occType)).getText().trim());
			rates.setAffiliateCommissionableAmount(driver.findElement(By.id("lblAffiliateCommissionable"+occType)).getText().trim());
			rates.setAffiliateCommissionAmount(driver.findElement(By.id("lblAffiliateCommission"+occType)).getText().trim());
			rates.setAffiliateNetProfit(driver.findElement(By.id("lblAffiliateNetProfit"+occType)).getText().trim());


			RateMapSaved.put(occType, rates);


		}

		CostingValidations Validationmap=new CostingValidations();

		for (int i = 0; i < type.length; i++) {

			Map<String, Integer> ratemap=new HashMap<String, Integer>();

			ratemap.put("Total", Integer.parseInt(driver.findElement(By.id("txtAdjustTotalCost"+type[i])).getAttribute("value")));
			ratemap.put("Profit Markup Amount", Integer.parseInt(driver.findElement(By.id("lblProfitMarkup"+type[i])).getText()));
			ratemap.put("Proposed Sell Rate", Integer.parseInt(driver.findElement(By.id("lblProposedSellRate"+type[i])).getText()));
			ratemap.put("Final Sell Rate", Integer.parseInt(driver.findElement(By.id("txtFinalSellRate"+type[i])).getAttribute("value")));
			ratemap.put("Agent Commissionable Amount", Integer.parseInt(driver.findElement(By.id("lblAgentCommissionable"+type[i])).getText()));
			ratemap.put("Agent Commission Amount", Integer.parseInt(driver.findElement(By.id("lblAgentCommission"+type[i])).getText()));
			ratemap.put("Agent Net Profit", Integer.parseInt(driver.findElement(By.id("lblNetProfit"+type[i])).getText()));
			ratemap.put("Affiliate Commissionable Amount", Integer.parseInt(driver.findElement(By.id("lblAffiliateCommissionable"+type[i])).getText()));
			ratemap.put("Affiliate Commission Amount", Integer.parseInt(driver.findElement(By.id("lblAffiliateCommission"+type[i])).getText()));
			ratemap.put("Affiliate Net Profit", Integer.parseInt(driver.findElement(By.id("lblAffiliateNetProfit"+type[i])).getText()));


			ActualMap.put(type[i], ratemap);
			ExpectedMap.put(type[i], Validationmap.FinalCosting(obj.packageName, type[i]));

		 }



		for (int i = 0; i < type.length; i++) {

			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Total")), String.valueOf(ActualMap.get(type[i]).get("Total")), "Total Cost for "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Profit Markup Amount")), String.valueOf(ActualMap.get(type[i]).get("Profit Markup Amount")), "Profit Markup Amount for "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Proposed Sell Rate")), String.valueOf(ActualMap.get(type[i]).get("Proposed Sell Rate")), "Proposed Sell Rate "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Final Sell Rate")), String.valueOf(ActualMap.get(type[i]).get("Final Sell Rate")), "Final Sell Rate "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Agent Commissionable Amount")), String.valueOf(ActualMap.get(type[i]).get("Agent Commissionable Amount")), "Agent Commissionable Amount "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Agent Commission Amount")), String.valueOf(ActualMap.get(type[i]).get("Agent Commission Amount")), "Agent Commission Amount "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Agent Net Profit")), String.valueOf(ActualMap.get(type[i]).get("Agent Net Profit")), "Agent Net Profit "+type[i]+" Validation"));

			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Affiliate Commissionable Amount")), String.valueOf(ActualMap.get(type[i]).get("Affiliate Commissionable Amount")), "Affiliate Commissionable Amount "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Affiliate Commission Amount")), String.valueOf(ActualMap.get(type[i]).get("Affiliate Commission Amount")), "Affiliate Commission Amount "+type[i]+" Validation"));
			PrintWriter.append(verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("Affiliate Net Profit")), String.valueOf(ActualMap.get(type[i]).get("Affiliate Net Profit")), "Affiliate Net Profit "+type[i]+" Validation"));
			verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("")), String.valueOf(ActualMap.get(type[i]).get("")), "");
			verifyTrue(String.valueOf(ExpectedMap.get(type[i]).get("")), String.valueOf(ActualMap.get(type[i]).get("")), "");

		 }


		Map<String , Map<String, RateObj>>Ratemaps=new HashMap<String, Map<String,RateObj>>();

		CostingValidations validator=new CostingValidations();
		Map<String, RateObj>Expected=validator.RateMapGenerator(obj.packageName, filePath);
		Ratemaps.put("Generated", RateMapGenerated);
		Ratemaps.put("Saved", RateMapSaved);
		Ratemaps.put("Expected", Expected);

		ResultsLoader loader=new ResultsLoader();
		loader.resultsGenerator(Ratemaps,obj.packageName);


		PrintWriter.append("</body></html>");
		Reportwriter report=new Reportwriter();
		String location=report.reportwrite();
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(location+"FP_Costing_"+obj.packageName+".html")));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();



		return PrintWriter;



		 */}

	public String DateCalculator(String date,int increment) throws ParseException {

		Calendar cal=Calendar.getInstance();
		cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
		cal.add(Calendar.DATE, increment);
		String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		return fromDate; 
	}



	public void deletePackage(WebDriver driver, standardinfoObject obj) {


		driver.get(obj.url+"/packaging/setup/PackageSetupPage.do?module=contract");

		driver.findElement(By.id("screenAction_delete")).click();
		driver.findElement(By.id("packageName")).sendKeys(obj.packageName);
		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().frame("lookup");
		((JavascriptExecutor)driver).executeScript("selectedRecord(0,'');");

		driver.switchTo().defaultContent();

		((JavascriptExecutor)driver).executeScript("beforeSave();");

		new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.id("dialogMsgActionButtons")));

		/*	driver.close();*/

	}


	public StringBuffer verifyTrue(String Expected,String Actual,String Message)
	{

		StringBuffer PrintWriter=new StringBuffer();
		PrintWriter.append("<tr><td>"+"1"+"</td><td>"+Message+"</td>");
		//	TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(Actual.trim().equalsIgnoreCase(Expected.trim()))
				PrintWriter.append("<td class='Passed'>PASS</td>");
			else
				PrintWriter.append("<td class='Failed'>FAIL</td>");

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;
	}


	public String GettheDate(String Date,String Type) throws ParseException {

		String returnObject = null;

		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Date date=new Date();
		date=sdf.parse(Date);


		if (Type.contains("d")) {


			returnObject=new SimpleDateFormat("dd").format(date);
		}
		if (Type.contains("m")) {

			String Month=new SimpleDateFormat("MMM").format(date);
			returnObject=Month;
		}
		if (Type.contains("y")) {
			returnObject=new SimpleDateFormat("yyyy").format(date);
		}




		return returnObject ;

	}

	public Map<String, costObj> ReadRates(WebDriver driver) {

		Map<String, costObj> RateMapGenerated=new HashMap<String, costObj>();

		String[] type={"Single","Double","Triple","Child"};

		for (int i = 0; i < type.length; i++) {

			costObj rates=new costObj();
			String occType=type[i];
			System.out.println(occType);

			try {
				rates.setAir(driver.findElements(By.id("air"+occType+"Value")).get(1).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				rates.setHotel(driver.findElements(By.id("hotel"+occType+"Value")).get(1).getText().trim());			

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				rates.setProgram(driver.findElements(By.id("program"+occType+"Value")).get(1).getText().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}

			rates.setTotalCost(driver.findElements(By.id("totalCost"+occType)).get(1).getText().trim());
			rates.setAdjustableTotalCost(driver.findElement(By.id("txtAdjustTotalCost"+occType)).getAttribute("value").trim());

			rates.setTotalTaxValue(driver.findElements(By.id("totalTax"+occType)).get(1).getText().trim());

			rates.setProfitMarkupAmount(driver.findElement(By.id("lblProfitMarkup"+occType)).getText().trim());
			rates.setProposedSellRate(driver.findElement(By.id("lblProposedSellRate"+occType)).getText().trim());
			rates.setFinalSellRate(driver.findElement(By.id("txtFinalSellRate"+occType)).getAttribute("value").trim());
			rates.setAgentCommissionableAmount(driver.findElement(By.id("lblAgentCommissionable"+occType)).getText().trim());
			rates.setAgentCommissionAmount(driver.findElement(By.id("lblAgentCommission"+occType)).getText().trim());
			rates.setAgentNetProfit(driver.findElement(By.id("lblNetProfit"+occType)).getText().trim());
			rates.setAffiliateCommissionableAmount(driver.findElement(By.id("lblAffiliateCommissionable"+occType)).getText().trim());
			rates.setAffiliateCommissionAmount(driver.findElement(By.id("lblAffiliateCommission"+occType)).getText().trim());
			rates.setAffiliateNetProfit(driver.findElement(By.id("lblAffiliateNetProfit"+occType)).getText().trim());


			RateMapGenerated.put(occType, rates);



		}
		return RateMapGenerated;

	}

	public static void main(String[] args) throws ParseException {

		Setup setup=new Setup();

		System.out.println(setup.GettheDate("2015-01-16", "y"));
		System.out.println(setup.DateCalculator("2015-01-16", -1));

	}






}
