package setup.com.pojo;

public class Currency {
	
	public  String  currencyCode      ="";
	public  String  CurrrencyName      ="";
	public  String  buyingRate           ="";
	public  String  sellingRate           ="";
	
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrrencyName() {
		return CurrrencyName;
	}
	public void setCurrrencyName(String currrencyName) {
		CurrrencyName = currrencyName;
	}
	public String getBuyingRate() {
		return buyingRate;
	}
	public void setBuyingRate(String buyingRate) {
		this.buyingRate = buyingRate;
	}
	public String getSellingRate() {
		return sellingRate;
	}
	public void setSellingRate(String sellingRate) {
		this.sellingRate = sellingRate;
	}
	
	
	

}
