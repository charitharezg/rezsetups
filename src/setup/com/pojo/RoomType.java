package setup.com.pojo;

public class RoomType {
	
	public  String Room_type   ="";
	public  String Room_type_Desc ="";
	public  String Room_Acc_Type = "";
	
	
	public String getRoom_type() {
		return Room_type;
	}
	public void setRoom_type(String room_type) {
		Room_type = room_type;
	}
	public String getRoom_type_Desc() {
		return Room_type_Desc;
	}
	public void setRoom_type_Desc(String room_type_Desc) {
		Room_type_Desc = room_type_Desc;
	}
	public String getRoom_Acc_Type() {
		return Room_Acc_Type;
	}
	public void setRoom_Acc_Type(String room_Acc_Type) {
		Room_Acc_Type = room_Acc_Type;
	}
	
	

}
