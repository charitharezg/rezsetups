package setup.runners.Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import setup.com.pojo.HotelBookingFee;
import setup.com.readers.PG_Properties;

public class BookingFeeSetupFlow {
	
	
	
	public WebDriver setBookingfeeSetup(WebDriver driver,HotelBookingFee bookingfee, String Base) throws InterruptedException {
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/admin/setup/BookingFeeSetupPage.do?module=contract");
		
		System.out.println("Succussfully loaded Hotel Booking Fee");
		
		
		//Select Country
		
		driver.findElement(By.id("countryName")).clear();
		driver.findElement(By.id("countryName")).sendKeys(bookingfee.getCountryName());
		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Select City
		
		driver.findElement(By.id("cityName")).clear();
		driver.findElement(By.id("cityName")).sendKeys(bookingfee.getCityName());
		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Tax & Fee Type
		
		if (bookingfee.getTax().equals("Percentage")) {

			driver.findElement(By.id("feeType_P")).click();
		} else {
			driver.findElement(By.id("feeType_V")).click();
		}
		
		//Amount
		
		driver.findElement(By.id("amount")).clear();
		driver.findElement(By.id("amount")).sendKeys(bookingfee.getAmount());
		
		driver.findElement(By.id("saveButId")).click();
		
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		
		
		
		
		
		return driver;
	}
	}
	
	
	

