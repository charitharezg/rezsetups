package setup.runners.Flow;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.Discount;


public class DiscountSetupFlow {
	
	
	
	
	public WebDriver setUpDiscount(WebDriver driver , Discount discount , String BaseUrl, StringBuffer ReportPrinter ){
 
    try {
    	
	
		
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>###</th>"
				+ "<th>Coupon Name</th>"
				+ "<th>Coupon number</th>"
				+ "<th>Errors</th>"
				+ "<th>Status</th></tr>");

						String discountName 		= discount.getDiscountName();
						String status				= discount.getDiscountstatus();
						String bookingChannel 		= discount.getBookingChannel();
						String partner				= discount.getPartner();
						String discountType 		= discount.getDiscountType();
						String discountv			= discount.getDiscount();
						String validityPeriod		= discount.getValidityPeriod();
						String bookingDateFrom		= discount.getBookingDateFrom();
						String bookingDateTo		= discount.getBookingDateTo();
						String checkinFrom			= discount.getCheckinFrom();
						String checkinTo			= discount.getCheckinTo();
						String discountApplicableOn	= discount.getDiscountApplicableOn();
						String date					= discount.getDate();
						String productType			= discount.getProductType();
						String couponType			= discount.getCouponType();
						String component			= discount.getComponent();
						// edited for picc
						String fullOrBookingFee		= "N/A";
						String country				= discount.getCountry();
						String country2				= discount.getCountry2();
						String allOrSelected		= discount.getAllOrSelected();
						String city					= discount.getCity();
						String singleCoupon			= discount.getSingleCoupon();
						String sequenceCoupon		= discount.getSequenceCoupon();
						String hotels				= discount.getHotels();
						String activities			= discount.getActivities();
						String car					= discount.getCar();
						String numOfCoupons			= discount.getNumOfCoupons();
						String reusability			= discount.getReusability();
						String numOfTimes			= discount.getNumOfTimes();
						String execute 				= discount.getExecute();
						String diactivate			= discount.getDiactivate();
						String checkAvailability  	= discount.getCheckAvailability();
						String count				= discount.getCount();
						String modify				= discount.getModify();
						
						String saveError = "N/A";
						String discountstatus = "";
						
						//System.out.print(discountName.concat("'"));
						
						if(execute.equals("yes") && diactivate.equals("no") && modify.equals("no"))
						{
							
							//Navigate to discount coupon creatin form
						    driver.get(BaseUrl.concat("/admin/setup/SetupDiscountCouponPage.do?actiontype=resetFormData&screenAction=create"));
							//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("moduleimage")));
							driver.findElement(By.id("discountName")).sendKeys(discountName);
							//select discount status
							if(status.equals("active"))
							{
								driver.findElement(By.id("activeStatus_Y")).click();
							}
							else if(status.equals("inactive"))
							{
								driver.findElement(By.id("activeStatus_N")).click();
							}
							//Select booking type
							if(bookingChannel.equals("all"))
							{
								driver.findElement(By.id("bookingChannel_ALL")).click();
							}
							else if(bookingChannel.equals("cc"))
							{
								driver.findElement(By.id("bookingChannel_CC")).click();
							}
							else if(bookingChannel.equals("web"))
							{
								driver.findElement(By.id("bookingChannel_WEB")).click();
							}
							//select partner
							if(partner.equals("all"))
							{
								driver.findElement(By.id("partnerType_ALL")).click();
							}
							else if(partner.equals("direct"))
							{
								driver.findElement(By.id("partnerType_DC")).click();
							}
							else if(partner.equals("to"))
							{
								driver.findElement(By.id("partnerType_TO")).click();
							}
							//discount type
							if(discountType.equals("percentage"))
							{
								driver.findElement(By.id("discountType_P")).click();
							}
							else if(discountType.equals("fixed"))
							{
								driver.findElement(By.id("discountType_V")).click();
							}
							//discount amount
							driver.findElement(By.id("discount")).clear();
							driver.findElement(By.id("discount")).sendKeys(discountv);
							//validity period
							String[] dateTemp = null;
							if(validityPeriod.equals("both"))
							{
								driver.findElement(By.id("basedOnBookingDate")).click();
								dateTemp = bookingDateTo.split("-");
								new Select(driver.findElement(By.id("bookingToDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								new Select(driver.findElement(By.id("bookingToDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								new Select(driver.findElement(By.id("bookingToDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								dateTemp = bookingDateFrom.split("-");
								new Select(driver.findElement(By.id("bookingFromDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("bookingFromDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("bookingFromDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								try {
									driver.findElement(By.id("dialogMsgText")).getText();
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								driver.findElement(By.id("basedOnCheckinDepartureDate")).click();
								dateTemp = checkinTo.split("-");
								new Select(driver.findElement(By.id("checkinDepartureToDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								new Select(driver.findElement(By.id("checkinDepartureToDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								new Select(driver.findElement(By.id("checkinDepartureToDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								dateTemp = checkinFrom.split("-");
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Year_ID"))).selectByVisibleText(dateTemp[2]);	
								try {
									driver.findElement(By.id("dialogMsgText")).getText();
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
							else if(validityPeriod.equals("bookingDate"))
							{
								driver.findElement(By.id("basedOnBookingDate")).click();
								dateTemp = bookingDateTo.split("-");
								//System.out.println(dateTemp[2]);
								new Select(driver.findElement(By.id("bookingToDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								new Select(driver.findElement(By.id("bookingToDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								new Select(driver.findElement(By.id("bookingToDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								dateTemp = bookingDateFrom.split("-");
								
								new Select(driver.findElement(By.id("bookingFromDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("bookingFromDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								
								new Select(driver.findElement(By.id("bookingFromDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								try {
									driver.findElement(By.id("dialogMsgText")).getText();
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
							else if(validityPeriod.equals("checkin"))
							{
								driver.findElement(By.id("basedOnCheckinDepartureDate")).click();
								dateTemp = checkinTo.split("-");
								new Select(driver.findElement(By.id("checkinDepartureToDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								new Select(driver.findElement(By.id("checkinDepartureToDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								new Select(driver.findElement(By.id("checkinDepartureToDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								dateTemp = checkinFrom.split("-");
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
								new Select(driver.findElement(By.id("checkinDepartureFromDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
								try {
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
							
							//discount applicable on
							if(discountApplicableOn.equals("bookingDate"))
							{
								driver.findElement(By.id("discountApplicableDaysOn_B")).click();
							}
							else if(discountApplicableOn.equals("checkIn"))
							{
								driver.findElement(By.id("discountApplicableDaysOn_C")).click();
							}
							//select days of the week
							dateTemp = date.split("/");
							for(int i = 0 ; i < dateTemp.length ; i++)
							{
								driver.findElement(By.id(dateTemp[i])).click();
							}
							
							//select product
							if(productType.equals("package"))
							{
								driver.findElement(By.id("discountFor_P")).click();
								if(component.equals("land"))
								{
									driver.findElement(By.id("pkgDiscountFor_LO")).click();
								}
								else if(component.equals("all"))
								{
									driver.findElement(By.id("pkgDiscountFor_ALL")).click();
								}
								driver.findElement(By.id("countryName")).clear();
								driver.findElement(By.id("countryName")).sendKeys(country);
								driver.findElement(By.id("countryName_lkup")).click();
								driver.switchTo().frame("lookup");
								driver.findElement(By.className("rezgLook0")).click();
								driver.switchTo().defaultContent();
								
								if(allOrSelected.equals("all"))
								{
									driver.findElement(By.id("city_ALL")).click();
								}
								else if(allOrSelected.equals("selected"))
								{
									driver.findElement(By.id("city_SPE")).click();
								}
								driver.findElement(By.id("addPkgDetailButton")).click();
							}
							else
							{
								driver.findElement(By.id("discountFor_I")).click();
								//System.out.println(productType);
								if(productType.equals("flight"))
								{
									driver.findElement(By.id("discountModule_F")).click();
									if(fullOrBookingFee.equals("full"))
									{
										driver.findElement(By.id("flightdiscounton_FF")).click();
									}
									else if(fullOrBookingFee.equals("bookingFee"))
									{
										driver.findElement(By.id("flightdiscounton_BF")).click();
									}
									driver.findElement(By.id("flightArrCountyName")).clear();
									driver.findElement(By.id("flightArrCountyName")).sendKeys(country);
									driver.findElement(By.id("flightArrCountyName_lkup")).click();
									driver.switchTo().frame("lookup");
									driver.findElement(By.className("rezgLook0")).click();
									driver.switchTo().defaultContent();
									
									ArrayList<WebElement> addButton = new ArrayList<WebElement>();
									WebElement aa;
									aa = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr[6]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td"));
									addButton.addAll(aa.findElements(By.id("addPkgDetailButton")));
									//System.out.println(addButton.size());
									if(allOrSelected.equals("all"))
									{
										driver.findElement(By.id("flightArrCity_ALL")).click();
									}
									else if(allOrSelected.equals("selsected"))
									{
										driver.findElement(By.id("flightArrCity_SPE")).click();
										driver.findElement(By.id("viewCityButton")).click();
										//select city
										addButton.get(0).click();
									}
									addButton.get(0).click();
									driver.findElement(By.id("flightDepCountyName")).clear();
									driver.findElement(By.id("flightDepCountyName")).sendKeys(country2);
									driver.findElement(By.id("flightDepCountyName_lkup")).click();
									driver.switchTo().frame("lookup");
									driver.findElement(By.className("rezgLook0")).click();
									driver.switchTo().defaultContent();
									
									if(allOrSelected.equals("all"))
									{
										driver.findElement(By.id("flightDepCity_ALL")).click();
									}
									else if(allOrSelected.equals("selected"))
									{
										driver.findElement(By.id("flightDepCity_SPE")).click();
										ArrayList<WebElement> view = new ArrayList<WebElement>();
										view.addAll(driver.findElements(By.id("viewCityButton")));
										view.get(view.size()-1).click();
										addButton.get(1).click();
									}
									aa = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr[6]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td"));
									addButton.addAll(aa.findElements(By.id("addPkgDetailButton")));
									((JavascriptExecutor)driver).executeScript("$('#addPkgDetailButton').trigger('click');");	
								}
								else if(productType.equals("hotel"))
								{
									driver.findElement(By.id("discountModule_H")).click();
									if(country.equals("all"))
									{
										
									}
									else
									{
										driver.findElement(By.id("hotelCountyName")).clear();
										driver.findElement(By.id("hotelCountyName")).sendKeys(country);
										driver.findElement(By.id("hotelCountyName_lkup")).click();
										driver.switchTo().frame("lookup");
										driver.findElement(By.className("rezgLook0")).click();
										driver.switchTo().defaultContent();
									}
									if(city.equals("all"))
									{
										
									}
									else
									{
										driver.findElement(By.id("hotelCityName")).clear();
										driver.findElement(By.id("hotelCityName")).sendKeys(city);
										driver.findElement(By.id("hotelCityName_lkup")).click();
										driver.switchTo().frame("lookup");
										driver.findElement(By.className("rezgLook1")).click();
										driver.switchTo().defaultContent();
									}
									
									if(allOrSelected.equals("all"))
									{
										driver.findElement(By.id("hotels_ALL")).click();
									}
									else if(allOrSelected.equals("selected"))
									{
										driver.findElement(By.id("hotels_SPE")).click();
										driver.findElement(By.id("viewHotelButton")).click();
										driver.switchTo().frame("dialogwindow");
										String[] hotelList = hotels.split("/");
										ArrayList<String> hList = new ArrayList<String>();
										for(int i = 0 ; i < hotelList.length ; i++)
										{
											hList.add(hotelList[i]);
										}
										String hName = "";
										for(int i = 1 ; i > 0 ; i ++)
										{
											try {
												hName = driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td")).getText();
												if(hList.contains(hName))
												{
													driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td/input")).click();
												}
											} catch (Exception e) {
												// TODO: handle exception
												driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
												break;
											}
										}
									}
									driver.findElement(By.id("addHotelDetailButton")).click();
									driver.switchTo().defaultContent();
					 			}
								else if(productType.equals("activity"))
								{
									driver.findElement(By.id("discountModule_A")).click();
									if(country.equals("all"))
									{
										driver.findElement(By.id("programCountyName")).clear();
										driver.findElement(By.id("programCountyName")).sendKeys(country);
										driver.findElement(By.id("programCountyName_lkup")).click();
										driver.switchTo().frame("lookup");
										driver.findElement(By.className("rezgLook0")).click();
										driver.switchTo().defaultContent();
									}
									else
									{
										
									}
									
									if(city.equals("all"))
									{
										
									}
									else
									{
										driver.findElement(By.id("programCityName")).clear();
										driver.findElement(By.id("programCityName")).sendKeys(city);
										driver.findElement(By.id("programCityName_lkup")).click();
										driver.switchTo().frame("lookup");
										driver.findElement(By.className("rezgLook1")).click();
										driver.switchTo().defaultContent();
									}
									
									if(allOrSelected.equals("all"))
									{
										driver.findElement(By.id("programs_ALL")).click();
									}
									else if(allOrSelected.equals("selected"))
									{
										driver.findElement(By.id("programs_SPE")).click();
										driver.findElement(By.id("viewProgramButton")).click();
										driver.switchTo().frame("dialogwindow");
										String[] actList = activities.split("/");
										ArrayList<String> aList = new ArrayList<String>();
										for(int i = 0 ; i < actList.length ; i++)
										{
											aList.add(actList[i]);
										}
										String aName = "";
										for(int i = 1 ; i > 0 ; i ++)
										{
											try {
												aName = driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td")).getText();
												if(aList.contains(aName))
												{
													driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td/input")).click();
												}
											} catch (Exception e) {
												// TODO: handle exception
												driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
												break;
											}
											
										}
									}
									driver.findElement(By.id("addProgramDetailButton")).click();	
									driver.switchTo().defaultContent();
								}
								else if(productType.equals("car"))
								{
									driver.findElement(By.id("discountModule_C")).click();
									if(fullOrBookingFee.equals("full"))
									{
										driver.findElement(By.id("carDiscountOn_CR")).click();
									}
									else if(fullOrBookingFee.equals("bookingFee"))
									{
										driver.findElement(By.id("carDiscountOn_BF")).click();
									}
									driver.findElement(By.id("carCountyName")).clear();
									driver.findElement(By.id("carCountyName")).sendKeys(country);
									driver.findElement(By.id("carCountyName_lkup")).click();
									driver.switchTo().frame("lookup");
									driver.findElement(By.className("rezgLook0")).click();
									driver.switchTo().defaultContent();
									if(allOrSelected.equals("all"))
									{
										driver.findElement(By.id("carCity_ALL")).click();
									}
									else if(allOrSelected.equals("selected"))
									{
										String[] carList = car.split("/");
										ArrayList<String> cList = new ArrayList<String>();
										driver.findElement(By.id("carCity_SPE")).click();
										//driver.switchTo().defaultContent();
										((JavascriptExecutor)driver).executeScript("$('#viewCityButton').trigger('click');");
										for(int i = 0 ; i < carList.length ; i++)
										{
											cList.add(carList[i]);
										}
										String cName ="";
										for(int i = 1 ; i > 0 ; i++)
										{
											try {
												cName = driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td")).getText();
												if(cList.contains(cName))
												{
													driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[2]/tbody/tr["+ i +"]/td/input")).click();
												}
											} catch (Exception e) {
												// TODO: handle exception
												driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
												break;
											}
											
										}
									}
									driver.switchTo().defaultContent();
									try {
										Thread.sleep(1000);
										driver.findElements(By.id("addPkgDetailButton")).get(3).click();
									} catch (Exception e) {
										// TODO: handle exception
									}
									
								}
							}
							
							if(couponType.equals("single"))
							{
								driver.findElement(By.id("couponType_SI")).click();
								driver.findElement(By.id("couponNumber")).sendKeys(singleCoupon);
							}
							else if(couponType.equals("sequence"))
							{
								driver.findElement(By.id("couponType_SE")).click();  
								driver.findElement(By.id("couponStartingString")).sendKeys(sequenceCoupon);
								driver.findElement(By.id("noOfCouponRequired")).clear();
								driver.findElement(By.id("noOfCouponRequired")).sendKeys(numOfCoupons);
								if(reusability.equals("yes"))
								{
									driver.findElement(By.id("couponCanBeReused")).click();
								}
								new Select(driver.findElement(By.xpath(".//*[@id='idcouponcanbereused']/td[2]/table/tbody/tr/td[3]/select"))).selectByValue(numOfTimes);
							}
							driver.findElement(By.id("saveButId")).click();
							
							try {
								saveError = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
								driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
							} catch (Exception e) {
								// TODO: handle exception
							}
							//System.out.println(saveError);
							try {
								saveError = driver.findElement(By.id("dialogMsgText")).getText();
								driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
							} catch (Exception e) {
								// TODO: handle exception
							}
							//System.out.println(saveError);
							try {
								driver.findElement(By.xpath(".//*[@id='idgeneratebutton']/td/input")).click();
								driver.findElement(By.xpath("html/body/form/table/tbody/tr[3]/td/a")).click();
								driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a[2]/img")).click();
								driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr[4]/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[2]/input")).click();
								driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
							} catch (Exception e) {
								// TODO: handle exception
								try {
									driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr[4]/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[2]/input")).click();
									driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
									discountstatus = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr[4]/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[2]/b")).getText();
								} catch (Exception e2) {
									// TODO: handle exception
								}
								
							}
							//-----------------------------------------------------------------------------------------
							
							if(discountstatus.equals("Issued"))
							{
								ReportPrinter.append	(	"<tr><td>"+ discount.getDiscountName() +"</td>"
										+	"<td>"+ discountName +"</td>"
										+ 	"<td>"+ singleCoupon +"</td>"
										+ 	"<td>"+ saveError +"</td>"
										+	"<td class='passed'>"+ discountstatus +"</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+ discount.getDiscountName() +"</td>"
										+ 	"<td>"+ discountName +"</td>"
										+ 	"<td>"+ singleCoupon +"</td>"
										+ 	"<td>"+ saveError +"</td>"
										+	"<td class='Failed'>"+ discountstatus +"</td></tr>");
							}
						}
						else if(execute.equals("no") && diactivate.equals("yes") && modify.equals("yes"))
						{
							 driver.get(BaseUrl.concat("/admin/setup/SetupDiscountCouponPage.do?actiontype=resetFormData&screenAction=modify"));
							try {
								driver.findElement(By.id("discountName")).clear();
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println(e);
							}
							System.out.println(discountName);
							driver.findElement(By.id("discountName")).sendKeys(discountName);
							driver.findElement(By.id("discountName_lkup")).click();
							driver.switchTo().frame("lookup");
							driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
							driver.switchTo().defaultContent();
							driver.findElement(By.id("activeStatus_N")).click();
							driver.findElement(By.id("saveButId")).click();
							String modifyStatus = driver.findElement(By.xpath(".//*[@id='dialogMsgText']/b")).getText();
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();		
						}
						else if(checkAvailability.equals("yes") && execute.equals("no") && diactivate.equals("no") && modify.equals("no"))
						{
							 driver.get(BaseUrl.concat("/admin/setup/SetupDiscountCouponPage.do?actiontype=resetFormData&screenAction=modify"));
							try {
								driver.findElement(By.id("discountName")).clear();
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println(e);
							}
							driver.findElement(By.id("discountName")).sendKeys(discountName);
							driver.findElement(By.id("discountName_lkup")).click();
							driver.switchTo().frame("lookup");
							try {
								Thread.sleep(2000);
								driver.findElement(By.xpath(".//*[@id='row-0']/td")).getText();
								driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
								ReportPrinter.append("<br>");
								ReportPrinter.append("<span class='passed'> "+ discount.getDiscountName() + "-".concat(count.concat("-".concat(discountName.concat(" - passed"))))  +" <span>");
								System.out.println(discount.getDiscountName() + "-".concat(count.concat("-".concat(discountName.concat(" - passed")))) );
							} catch (Exception e) {
								// TODO: handle exception
								ReportPrinter.append("<br>");
								ReportPrinter.append("<span class='Failed'> "+ discount.getDiscountName() + "-".concat(count.concat("-".concat(discountName.concat(" - passed"))))  +" <span>");
								System.out.println(discount.getDiscountName() + "-".concat(discountName.concat(" - failed")));
							}
							driver.switchTo().defaultContent();
							
						}
						else if(execute.equals("no") && diactivate.equals("no") && modify.equals("yes"))
						{
							 driver.get(BaseUrl.concat("/admin/setup/SetupDiscountCouponPage.do?actiontype=resetFormData&screenAction=modify"));
							try {
								WebDriverWait wait	 			= new WebDriverWait(driver, 20);
								wait.until(ExpectedConditions.presenceOfElementLocated(By.id("discountName")));
							} catch (Exception e) {
								// TODO: handle exception
							}
							try {
								driver.findElement(By.id("discountName")).clear();
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println(e);
							}
							driver.findElement(By.id("discountName")).sendKeys(discountName);
							driver.findElement(By.id("discountName_lkup")).click();
							Thread.sleep(1000);
							driver.switchTo().frame("lookup");
							try {
								try {
									WebDriverWait wait	 			= new WebDriverWait(driver, 20);
									wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='row-0']/td")));
								} catch (Exception e) {
									// TODO: handle exception
								}
								driver.findElement(By.xpath(".//*[@id='row-0']/td")).getText();
								driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
								ReportPrinter.append("<br>");
								ReportPrinter.append("<span>"+ discountName +"</span>");
								System.out.println(discount.getDiscountName() + "-".concat(count.concat("-".concat(discountName.concat(" - passed")))) );
							} catch (Exception e) {
								// TODO: handle exception
								System.out.println(discount.getDiscountName() + "-".concat(discountName.concat(" - failed")));
							}
							driver.switchTo().defaultContent();
	//*********************************************change the booking date******************************************************						
							/*String[] dateTemp = bookingDateTo.split("-");
							new Select(driver.findElement(By.id("bookingToDate_Year_ID"))).selectByVisibleText(dateTemp[2]);
							new Select(driver.findElement(By.id("bookingToDate_Month_ID"))).selectByVisibleText(dateTemp[1]);
							new Select(driver.findElement(By.id("bookingToDate_Day_ID"))).selectByVisibleText(dateTemp[0]);
							driver.findElement(By.id("saveButId")).click();
							try {
								driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
							} catch (Exception e) {
								// TODO: handle exception
							}
							System.out.println("done");*/
	//********************************************issue coupons*******************************************************************						
						}
					} catch (Exception e) {
	
					}
	
		return driver;
		
	}

}
