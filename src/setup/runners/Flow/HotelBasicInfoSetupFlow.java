package setup.runners.Flow;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.*;

public class HotelBasicInfoSetupFlow {
WebDriver driver  = null;
String    BaseUrl = "";
private Logger BasicLogger = null;
public HotelBasicInfoSetupFlow(String Base,WebDriver driver)
{
BaseUrl     = Base;	
this.driver = driver;
BasicLogger      = Logger.getLogger(this.getClass());
}
	

	
	//Create
	public WebDriver createHotelBeds(BedType bedtype) throws InterruptedException {
	
		driver.get(BaseUrl.concat("/hotels/setup/BedTypePage.do?module=contract"));
		Thread.sleep(500);	
		
		driver.findElement(By.xpath(".//*[@id='screenaction_create']")).click();
		Thread.sleep(500);	
		
		driver.findElement(By.id("bedTypeName")).sendKeys(bedtype.getBedTypeName());
		driver.findElement(By.id("defaultAdults")).sendKeys(bedtype.getBed_DefaultAdults());
		driver.findElement(By.id("bedTypeDescription")).sendKeys(bedtype.getBed_Desc());
		
		Thread.sleep(1000);		
		
		driver.findElement(By.id("saveButId")).click();		
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(bedtype.getBedTypeName() +" BedType SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(bedtype.getBedTypeName() +" BedType SetUp Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(bedtype.getBedTypeName() +" BedType SetUp - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		return driver;
								
	}
	
	
	
	public WebDriver createHotelGroup(HotelGroup hotelgroup) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/HotelGroupPage.do?module=contract");
		driver.findElement(By.xpath(".//*[@id='screenaction_create']")).click();
		Thread.sleep(500);	
		driver.findElement(By.id("hotelgroupName")).sendKeys(hotelgroup.getHotelGroup());
	    driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(hotelgroup.getHotelGroup() +"HotelGroup SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(hotelgroup.getHotelGroup() +"HotelGroup SetUp Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(hotelgroup.getHotelGroup() +"HotelGroup SetUp Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		return driver;
	}
	
	
	
	
	public WebDriver createRatePlan(RatePlan rateplan) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/SetupRatePlanPage.do?module=contract");
		driver.findElement(By.xpath(".//*[@id='screenaction_create']")).click();
		Thread.sleep(500);	
		driver.findElement(By.id("ratePlanName")).sendKeys(rateplan.getHotelRate());		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(rateplan.getHotelRate() +" Hotel Rate SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(rateplan.getHotelRate() +" Hotel Rate SetUp Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(rateplan.getHotelRate() +" Hotel Rate SetUp Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		return driver;
	}
	
	
	
	
	public WebDriver createRoomType(RoomType roomtype) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/RoomTypeDetailsPage.do?module=contract");
		driver.findElement(By.xpath(".//*[@id='screenaction_create']")).click();
		Thread.sleep(500);	
	    driver.findElement(By.id("roomTypeName")).sendKeys(roomtype.getRoom_type());
		driver.findElement(By.id("roomTypeDesc")).sendKeys(roomtype.getRoom_type_Desc());
		
		if (roomtype.getRoom_Acc_Type().equals("Hotels")) {
			
			driver.findElement(
					By.xpath(".//*[@id='accommodationType_HT']"))
					.click();
						
		} else {
			driver.findElement(
					By.xpath(".//*[@id='accommodationType_HS']"))
					.click();
		}
		
				
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(roomtype.getRoom_type() +" RoomType SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(roomtype.getRoom_type() +" RoomType SetUp  Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(roomtype.getRoom_type() +" RoomType SetUp  Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		return driver;
	}
	
	
		
	public WebDriver createStarCategory(StarCategory starcat) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/SetupStarCategoryPage.do?module=contract");
	    driver.findElement(By.id("starCategoryName")).sendKeys(starcat.getHotel_star());
		driver.findElement(By.id("categoryOrder")).sendKeys(starcat.getHotel_star_order());
				
		Thread.sleep(500);
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(starcat.getHotel_star() +" StarCat SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(starcat.getHotel_star() +" StarCat SetUp Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(starcat.getHotel_star() +" StarCat SetUp Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		return driver;
		
	}
	
	
	//Modify
	public WebDriver modifyHotelBeds(BedType bedtype) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/BedTypePage.do?module=contract");
		Thread.sleep(1500);
			
		driver.findElement(By.id("bedTypeName")).sendKeys(bedtype.getBedTypeName());
		driver.findElement(By.id("bedTypeName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		
		driver.findElement(By.id("defaultAdults")).clear();				
		driver.findElement(By.id("defaultAdults")).sendKeys("2");
		driver.findElement(By.id("bedTypeDescription")).clear();
		driver.findElement(By.id("bedTypeDescription")).sendKeys("Modify11");
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(bedtype.getBedTypeName() +" BedType Modify Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(bedtype.getBedTypeName() +" BedType Modify Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(bedtype.getBedTypeName() +" BedType Modify Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
				
		return driver;
		
	}
	
	public WebDriver modifyRoomType(RoomType roomtype) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/RoomTypeDetailsPage.do?module=contract");
		Thread.sleep(1500);
		
		driver.findElement(By.id("roomTypeName")).sendKeys(roomtype.getRoom_type());
		driver.findElement(By.id("roomTypeName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		
		driver.findElement(By.id("roomTypeDesc")).clear();
		driver.findElement(By.id("roomTypeDesc")).sendKeys("Modify11");
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
				
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(roomtype.getRoom_type() +" RoomType Modify Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(roomtype.getRoom_type() +" RoomType Modify Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(roomtype.getRoom_type() +" RoomType Modify - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		return driver;
	}
	
	
	public WebDriver modifyStarCategory(StarCategory starcategory) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/SetupStarCategoryPage.do?module=contract");
		Thread.sleep(1000);
		
		driver.findElement(By.id("starCategoryName")).sendKeys(starcategory.getHotel_star());
		driver.findElement(By.id("starCategoryName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();		
		
		driver.findElement(By.id("categoryOrder")).sendKeys("5");
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(starcategory.getHotel_star() +" StarCat Modify  Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(starcategory.getHotel_star() +" StarCat Modify  Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(starcategory.getHotel_star() +" StarCat Modify  Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		return driver;
	}
	
	
	//Delete
	public WebDriver deleteHotelBeds(BedType bedtype) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/BedTypePage.do?module=contract");
		Thread.sleep(500);	
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(500);
		
		driver.findElement(By.id("bedTypeName")).sendKeys(bedtype.getBedTypeName());
		driver.findElement(By.id("bedTypeName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(bedtype.getBedTypeName() +" BedType Delete Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(bedtype.getBedTypeName() +" BedType Delete Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(bedtype.getBedTypeName() +" BedType Delete Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
				
		return driver;
	}
	
	public WebDriver deleteHotelGroup(HotelGroup hotelgroup) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/HotelGroupPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(500);
		
		driver.findElement(By.id("hotelgroupName")).sendKeys(hotelgroup.getHotelGroup());
		driver.findElement(By.id("hotelgroupName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(hotelgroup.getHotelGroup() +"HotelGroup DeletionStatus - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(hotelgroup.getHotelGroup() +"HotelGroup Deletion Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(hotelgroup.getHotelGroup() +"HotelGroup Deletion Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
				
		return driver;
	}
	
	public WebDriver deleteHotelRatePlan(RatePlan rateplan) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/SetupRatePlanPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(500);
		
		driver.findElement(By.id("ratePlanName")).sendKeys(rateplan.getHotelRate());
		driver.findElement(By.id("ratePlanName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(rateplan.getHotelRate() +" Hotel Rate SetUp Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(rateplan.getHotelRate() +" Hotel Rate SetUp Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(rateplan.getHotelRate() +" Hotel Rate SetUp Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
				
		return driver;
	}
	
	public WebDriver deleteHotelRoom(RoomType roomtype) throws InterruptedException{
		
		driver.get(BaseUrl + "/hotels/setup/RoomTypeDetailsPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(500);
		
		driver.findElement(By.id("roomTypeName")).sendKeys(roomtype.getRoom_type());
		driver.findElement(By.id("roomTypeName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(roomtype.getRoom_type() +" RoomType Deletion Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(roomtype.getRoom_type() +" RoomType Deletion Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(roomtype.getRoom_type() +" RoomType Deletion Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}		
		return driver;
	}
	
	public WebDriver deleteHotelStarCat(StarCategory starcat) throws InterruptedException{
		
		driver.get(BaseUrl+ "/hotels/setup/SetupStarCategoryPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(500);
		
		driver.findElement(By.id("starCategoryName")).sendKeys(starcat.getHotel_star());
		driver.findElement(By.id("starCategoryName_lkup")).click();
		Thread.sleep(1000);
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		  WebDriverWait wait = new WebDriverWait(driver, 60);
			
			 try {
				    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
				   
				    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
				    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						System.out.println(TextMessage);
						BasicLogger.info(starcat.getHotel_star() +" StarCat Deletion Status - DONE  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				    }
				    else
				    {
				    	
				    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						System.out.println(TextMessage);
						BasicLogger.fatal(starcat.getHotel_star() +" StarCat Deletion Status - Failed  Message Out :----->"+TextMessage);
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				    }
				    
				} catch (Exception e) {
					BasicLogger.fatal(starcat.getHotel_star() +" StarCat Deletion Status - - Failed  Message Out :----->"+e.toString());
					System.out.println(e.toString());
					
				}
		
		
		return driver;
	}
	
	
	public void tearDown() {
		// driver.quit();
	}
}
