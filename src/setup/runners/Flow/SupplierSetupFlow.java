package setup.runners.Flow;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import setup.com.pojo.Currency;
import setup.com.pojo.Supplier;


public class SupplierSetupFlow {
	boolean ResultsRecorded = false;
	Logger logger = null;
	public SupplierSetupFlow ()
	{
		logger = Logger.getLogger(this.getClass());
	}

	public WebDriver CreatePage(WebDriver driver,Supplier sup, String Base) throws InterruptedException {
		driver.get(Base+"/admin/setup/SupplierSetupStandardPage.do?module=contract");

	   driver.findElement(By.id("screenaction_create")).click();
	   driver.findElement(By.id("suppliercode")).sendKeys(sup.getSupplierCode());
		
		driver.findElement(By.id("suppliername")).sendKeys(
				sup.getSupplierName());
		
		
/*	try {
		if (sup.getSupplierType().equals("Prepaid")) {

			driver.findElement(By.id("suppliertype_P")).click();
		} else {
			driver.findElement(By.id("suppliertype_C")).click();
		}
	} catch (Exception e) {
		// TODO: handle exception
	}*/
		
		
		driver.findElement(By.id("addressline1")).sendKeys(sup.getAddress().trim());
		
		//Select the Country
		driver.findElement(By.id("country")).sendKeys(sup.getCountry());
		driver.findElement(By.id("country_lkup")).click();;
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		

		// select the city
		driver.findElement(By.id("city")).sendKeys(sup.getCity());
		driver.findElement(By.id("city_lkup")).click();
		driver.switchTo().frame(driver.findElement(By.id("lookup")));
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		//WebDriverWait Mywait6 = new WebDriverWait(driver, 60);
		driver.switchTo().defaultContent();
		
		if (sup.isActive()) {

			driver.findElement(By.id("supplieractive_Y")).click();
		} else {
			driver.findElement(By.id("supplieractive_N")).click();
		}
		
		// select the Currency
		
		driver.findElement(By.id("currency")).sendKeys(sup.getCurrency());
		driver.findElement(By.id("currency_lkup")).click();
		driver.switchTo().frame(driver.findElement(By.id("lookup")));
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		
		//Add new Contact
		
		driver.switchTo().frame("suppliercontactdetails");
		driver.findElement(By.id("contactDetailsentry")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");
		driver.findElement(By.id("contactname")).sendKeys(sup.getContactName());
		driver.findElement(By.id("email")).sendKeys(sup.getEmail());
		
		
		if (sup.getContactMedia().equals("Email")) {

			driver.findElement(By.id("confirmationtype_E")).click();
			
		} else if(sup.getContactMedia().equals("Fax")){
			
			driver.findElement(By.id("confirmationtype_F")).click();
		}
		else if(sup.getContactMedia().equals("Phone")){
			
			driver.findElement(By.id("confirmationtype_P")).click();
		}
		else{
			driver.findElement(By.id("confirmationtype_N")).click();
		}
		

		new Select(driver.findElement(By.name("contactType")))
		.selectByVisibleText(sup.getContactType().trim());
		
		driver.findElement(By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
		driver.switchTo().defaultContent();
		
		
		
		driver.findElement(By.name("submit")).click();
	//	driver.findElement(By.name("submit")).click();
	    WebDriverWait wait = new WebDriverWait(driver, 60);
	
		 try {
			    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
			   
			    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
			    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					System.out.println(TextMessage);
					logger.info(sup.getSupplierName() +" Supplier SetUp Status - DONE  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			    }
			    else
			    {
			    	
			    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					System.out.println(TextMessage);
					logger.fatal(sup.getSupplierName() +" Supplier SetUp Status - Failed  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
			    }
			    
			} catch (Exception e) {
				logger.fatal(sup.getSupplierName() +" Supplier SetUp Status - - Failed  Message Out :----->"+e.toString());
				System.out.println(e.toString());
				
			}
		
		//System.out.println("Completed");

		return driver;
	}

	public WebDriver setCurrencyPageSetup(WebDriver driver,Currency currency, String Base) throws InterruptedException {
		
		driver.get(Base + "/admin/setup/CurrencySetupPage.do?module=admin");

		try {
			
			driver.findElement(By.id("screenaction_create")).click();
			Thread.sleep(2000);
			
			driver.findElement(By.id("currencyCode")).sendKeys(currency.getCurrencyCode());
			driver.findElement(By.id("currencyName")).sendKeys(currency.getCurrrencyName());
			   
			driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
			WebDriverWait wait = new WebDriverWait(driver, 120);
			
			try {
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
			   
			    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
			    	
			    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					System.out.println(TextMessage);
					logger.info(currency.getCurrencyCode() +" Currency SetUp Status - DONE  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			    }
			    else{
			    	
			    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					System.out.println(TextMessage);
					logger.fatal(currency.getCurrencyCode() +" Currency SetUp Status - Failed  Message Out :----->"+TextMessage);
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
			    }
				
			} catch (Exception e) {
				logger.fatal(currency.getCurrencyCode() +" Currency SetUp Status - - Failed  Message Out :----->"+e.toString());
			}
			
		} catch (Exception e) {
			logger.fatal("Currency Setup page not loaded - - Failed  Message Out :----->"+e.toString());
			System.out.println(e.toString());
		}
		
		return driver;
	}
	
	
	public WebDriver setCurrencyExchangeRateSetup(WebDriver driver,Currency currency, String Base) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(Base + "/admin/setup/CurrencyExchangeRateSetupPage.do?module=admin");

		try {
			
			driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[1]/a/img")).click();
			Thread.sleep(2000);
			
			driver.switchTo().frame("dialogwindow");
			Thread.sleep(2000);	
			
			try {
				
				driver.findElement(By.id("currencyCode")).sendKeys(currency.getCurrencyCode());
				driver.findElement(By.xpath(".//*[@id='currencyCode_lkup']")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				driver.findElement(By.xpath(".//*[@id='exchangeRate']")).sendKeys(currency.getBuyingRate());
				driver.findElement(By.xpath(".//*[@id='sellingexchangerate']")).sendKeys(currency.getSellingRate());
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
				Thread.sleep(6000);
				
				logger.info(currency.getCurrencyCode() +" Currency ExchangeRate SetUp Status - DONE ----->");
				
								
			} catch (Exception e) {
				logger.fatal(currency.getCurrencyCode() +" Currency Exchange SetUp Status - - Failed  Message Out :----->"+e.toString());
			}
			
			
		} catch (Exception e) {
			logger.fatal("Currency Exchange page not loaded - - Failed  Message Out :----->"+e.toString());
			System.out.println(e.toString());
		}
		
		return driver;
	}
	
	
	//Discount flow driver	
	
	
	public boolean isElementPresent(By by, WebElement element, WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			element.findElement(by);
			return true;
		} catch (Exception ex) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

	

	

}
