package setup.runners.mainsetup;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import setup.com.dataObjects.standardinfoObject;
import setup.com.enumtypes.RateContractByType;
import setup.com.loaders.DataLoader;
import setup.com.loaders.LoadActivityBasicDetails;
import setup.com.loaders.LoadActivityDetails;
import setup.com.packageSetup.ContentPage;
import setup.com.packageSetup.Setup;
import setup.com.pojo.ActivityRatePlan;
import setup.com.pojo.ActivityStandard;
import setup.com.pojo.ActivityType;
import setup.com.pojo.AirConfig;
import setup.com.pojo.Currency;
import setup.com.pojo.Discount;
import setup.com.pojo.Hotel;
import setup.com.pojo.HotelBookingFee;
import setup.com.pojo.PeriodSetup;
import setup.com.pojo.ProgramType;
import setup.com.pojo.Supplier;
import setup.com.readers.PG_Properties;
import setup.com.readers.ReadExcel;
import setup.runners.Flow.ActivityBasicInfo;
import setup.runners.Flow.ActivitySetupInfo;
import setup.runners.Flow.AirConfigurationSetUpFlow;
import setup.runners.Flow.BasicSetUpFlow;
import setup.runners.Flow.BookingFeeSetupFlow;
import setup.runners.Flow.DiscountSetupFlow;
import setup.runners.Flow.HotelSetupFlow;
import setup.runners.Flow.SupplierSetupFlow;
import setup.com.dataObjects.standardinfoObject;

public class SetupRunner {

	private WebDriver driver;
	private FirefoxProfile prof = null;

	private Map<String, String> Propertymap;
	private Map<Integer, String> Createmap;
	private Map<Integer, String> Contractmap;
	private Map<Integer, String> Policymap;
	private Map<Integer, String> Taxmap;
	private Map<Integer, String> Occupancymap;
	private Map<Integer, String> PMmap;
	private Map<Integer, String> Suppliermap;
	private Map<Integer, String> Supplimentarymap;
	private Map<Integer, String> Promotionmap;
	private ArrayList<Supplier> SupplierList;
	private static Logger logger = null;
	private TreeMap<String, Hotel> HotelList;
	TreeMap<String,standardinfoObject> objlist=  new TreeMap<String,standardinfoObject>();
	private BasicSetUpFlow BasicDriver = null;
	private Map<Integer, String> Currencymap;
	private ArrayList<Currency> currencyList;
	
	private Map<Integer, String> Discountmap;
	private ArrayList<Discount> DiscountList ;
	 
	private Map<Integer, String> Bookingfeemap;
	private ArrayList<HotelBookingFee> Bookingfeelist;
	
	private Map<Integer, String> Airconfigmap;
	private ArrayList<AirConfig> Airconfiglist;
	
	//FP Data Maps
	
    private  Map<Integer, String>  FP_InfoMap;
	private Map<Integer, String> FP_AirMap;
	private Map<Integer, String> FP_HotelMap;
	private Map<Integer, String> FP_ActivityMap;
	private Map<Integer, String> FP_ContentMap;
	private Map<Integer, String> FP_ItinaryMap;
	private Map<Integer, String> FP_HotelrateMap;
	private Map<Integer, String> FP_ActRateMap;
	private Map<Integer, String> SupList;
//		private Map<Integer, String> Suppliermap;
	/*	private Map<Integer, String> Supplimentarymap;
	private Map<Integer, String> Promotionmap;*/
	
	
	
	//Activity Related Data structure
	
	JavascriptExecutor javaScriptExe = (JavascriptExecutor)driver;	
	private LoadActivityDetails detailsLoder;
	private LoadActivityBasicDetails basicDetailsloader;
	private int i=0;
	
	ArrayList<Map<Integer, String>> ActivitySheetlist = new ArrayList<Map<Integer, String>>(); 
	ArrayList<Map<Integer, String>> ActivitySheetlist1 = new ArrayList<Map<Integer, String>>();
	ArrayList<ActivityStandard>  	ActivityStandardTypes  	= new ArrayList<ActivityStandard>();
	
	ArrayList<ProgramType>  	ProgramTypes  	= new ArrayList<ProgramType>();
	ArrayList<ActivityType>   	ActivityTypes	= new ArrayList<ActivityType>();	
	ArrayList<PeriodSetup>     	PeriodSetups    = new ArrayList<PeriodSetup>();
	ArrayList<ActivityRatePlan>     	RatePlans       = new ArrayList<ActivityRatePlan>();
	
	Map<Integer, String> ActivityStandardMap 	= null;
	Map<Integer, String> ActivityContractMap 			= null;
	Map<Integer, String> ActivityPoliciesMap 			= null;
	Map<Integer, String> ActivityTaxMap 				= null;
	Map<Integer, String> AssignActivityMap 		= null;
	Map<Integer, String> ActivityInventoryMap 	= null;
	Map<Integer, String> ActivityRatesMap 		= null;
	Map<Integer, String> ActivityProfitMap 		= null;
	
	
	private TreeMap<String, ActivityStandard>   ActivityList;
	
	private StringBuffer PrintWriter;
	private int TestCaseCount;
	
	
	
	standardinfoObject obj = new standardinfoObject();
	Setup setupmethods = new Setup();
	
	
//	private StringBuffer     ReprotPrinter;

	@Before
	public void setUp() throws Exception {
   //     ReprotPrinter = new StringBuffer();    
		logger        = Logger.getLogger(this.getClass());
		try {
			Propertymap = loadProperties("../Hotel_Setup_Details/Config.properties");
			DOMConfigurator.configure("ConfigLog/log4j.xml");
		} catch (Exception e) {
			logger.fatal("Error When Loading Properties");
			logger.fatal(e.toString());
		}

		/*if (Propertymap.get("Driver.Type").equals("Firefox")) {
			if (Propertymap.get("Profile.Use").equals("true"))
				prof = new FirefoxProfile(new File(
						Propertymap.get("Profile.Path")));
			else
				prof = new FirefoxProfile();
			driver = new FirefoxDriver(prof);
		} else {
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					Propertymap.get("Phantom.Bin"));
			driver = new PhantomJSDriver(cap);
		}*/
		
		String chromeFilePath = "E:/Projects/Drivers/chromedriver.exe";
   	    System.setProperty("webdriver.chrome.driver", chromeFilePath);
        driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if (Propertymap.get("Window.maximized").trim().equalsIgnoreCase("yes")) {
		driver.manage().window().maximize();	
		}
		loadHotelDetails();
		loadactivitydetails();
        loadFPDetails();
		
	}
	
	//fixpackage
	public void fixpackagesetupflow() throws Exception {
	
		HotelSetupFlow Flow = new HotelSetupFlow(Propertymap);
		Flow.loginPage(driver);
		
		List<standardinfoObject> list = new ArrayList<standardinfoObject>(objlist.values());
 		ContentPage ContentPageLoader = new ContentPage();
		
		for (int i = 7; i < 8; i++) {

			/*standardinfoObject obj = list.get(i);
			System.out.println("obj.TOPM" + obj.packageName);
			setupmethods.SatandardInfoPage(driver, obj);
			setupmethods.AssignProducts(driver, obj);

			if (obj.CostBy.contains("Predefined")) {

				PrintWriter.append(setupmethods.costing_New(driver, obj));
			}*/

			try {
				standardinfoObject obj = list.get(i);
				System.out.println("obj.TOPM" + obj.packageName);
                System.out.println("i=" + i + "and Package=" + obj.packageName);
				setupmethods.SatandardInfoPage(driver, obj);
                setupmethods.AssignProducts(driver, obj);
                ContentPageLoader.ContentPageFill(driver, obj);
                setupmethods.AssignPM(driver, obj);
				if (obj.CostBy.contains("Predefined")) {
                 PrintWriter.append(setupmethods.costing_New(driver, obj));
				}

			} catch (Exception e) {
				System.out.println(obj.packageName);
				System.out.println(e);
			}

		/*	if (obj.CostBy.contains("Predefined")) {

				PrintWriter.append(setupmethods.costing_New(driver, obj,
						filepath));
			}
			if (obj.CostBy.contains("Predefined")) {

				PrintWriter.append(setupmethods.costing_New(driver, obj,
						filepath));
			}

			try {
				setupmethods.deletePackage(driver, obj);
			} catch (Exception e) {
				System.out.println(obj.packageName);
				System.out.println(e);
			}*/

		}

	}


	//====================Load Hotel details=============== 
	public void loadHotelDetails() throws Exception {

		ArrayList<Map<Integer, String>> SheetList = null;
		ArrayList<Map<Integer, String>> SupList = null;
		
		ArrayList<Map<Integer, String>> BasicList = null;
		
		ArrayList<Map<Integer, String>> DisList = null;
		
		ReadExcel ReadExl = new ReadExcel();
		DataLoader Loader = new DataLoader();
		
		
		SheetList = ReadExl.init(Propertymap.get("Create.Excel.Path"));
		Createmap = SheetList.get(0);
		Contractmap = SheetList.get(1);
		Policymap = SheetList.get(2);
		Taxmap = SheetList.get(5);
		Occupancymap = SheetList.get(3);
		PMmap = SheetList.get(4);
		Supplimentarymap = SheetList.get(6);
		Promotionmap = SheetList.get(7);

		if (Propertymap.get("Hotel.Setup").trim().equalsIgnoreCase("enable")) {
		

			// Loading Hotel Details
			try {
				HotelList = Loader.loadHotelDetails(Createmap);
				logger.info("Hotel Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Contract Details

			try {
				HotelList = Loader.loadContractDetails(Contractmap, HotelList);
				logger.info("Hotel Contracts Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Contracts not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Policy Details
			try {
				HotelList = Loader.loadPolicyDetails(Policymap, HotelList);
				logger.info("Hotel policies Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Policies not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Tax Details

			try {
				HotelList = Loader.loadTaxDetails(Taxmap, HotelList);
				logger.info("Tax Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Tax Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Room Details

			try {
				HotelList = Loader.loadRoomDetails(Occupancymap, HotelList);
				logger.info("Hotel Room Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Room Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}
			// Loading Profit Markup Details

			try {
				HotelList = Loader.loadMarkupDetails(PMmap, HotelList);
				logger.info("Hotel Markup Details Loaded SuccessFully");
				logger.debug(HotelList);
			} catch (Exception e) {
				logger.fatal("Hotel Markup Details not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			if (Propertymap.get("Supplimentary.Enabled").equalsIgnoreCase(
					"enable")) {

				try {
					HotelList = Loader.loadSupplimentaryDetails(
							Supplimentarymap, HotelList);
					logger.info("Hotel Supplimentary Details Loaded SuccessFully");
					logger.debug(HotelList);
				} catch (Exception e) {
					logger.fatal("Hotel Supplimentary Details Loading Failed");
					logger.fatal(e.toString());
				}
			}

			if (Propertymap.get("Promotion.Enabled").equalsIgnoreCase("enable")) {
				try {
					HotelList = Loader
							.loadPromoDetails(Promotionmap, HotelList);
					logger.info("Hotel Promotion Details Loaded SuccessFully");
					logger.debug(HotelList);
				} catch (Exception e) {
					logger.fatal("Hotel Promotion Details not Loaded SuccessFully");
					logger.fatal(e.toString());
				}
			}
		}
		//}

		if (Propertymap.get("Supplier.Setup").trim().equalsIgnoreCase("enable")) {
			SupList = ReadExl.init(Propertymap.get("Supplier.Excel.Path"));
			Suppliermap = SupList.get(0);
			SupplierList = Loader.loadSupplierDetails(Suppliermap);
			// SupplierContractmap = SupList.get(2);
		}
		
		//Currency list
		
		if (Propertymap.get("Currency.Setup").trim().equalsIgnoreCase("enable")) {
			SupList = ReadExl.init(Propertymap.get("Supplier.Excel.Path"));
			Currencymap = SupList.get(1);
			currencyList = Loader.loadCurrencyDetails(Currencymap);
			
		}		
		
		//Booking fee list
		
		if (Propertymap.get("BookingFee.Setup").trim().equalsIgnoreCase("enable")) {
			SupList = ReadExl.init(Propertymap.get("Supplier.Excel.Path"));
			Bookingfeemap = SupList.get(2);
			Bookingfeelist = Loader.loadbookingfeedetails(Bookingfeemap);
			
		}
		
		//Air configuration setup
		
 		if (Propertymap.get("Airconfig.Setup").trim().equalsIgnoreCase("enable")) {
			SupList = ReadExl.init(Propertymap.get("Supplier.Excel.Path"));
			Airconfigmap = SupList.get(3);
			Airconfiglist = Loader.loadairconfigdetails(Airconfigmap);
			
		}
		
		
		
		
		//Discount list
		
		if (Propertymap.get("Discount.Setup").trim().equalsIgnoreCase("enable")) {
			DisList = ReadExl.init(Propertymap.get("Discountcoupon.Excel.Path"));
			Discountmap = DisList.get(0);
			DiscountList = Loader.loadDiscountDetails(Discountmap);
			
		}		
		
		
		
		
		
		
		
		
		
		

		if (Propertymap.get("Basic.Setup").trim().equalsIgnoreCase("enable")) {
			BasicList = ReadExl.init(Propertymap.get("Basic.Excel.Path"));
			BasicDriver = new BasicSetUpFlow();
			BasicDriver.setUp(Propertymap, BasicList);
		}
		
		
	}
	//====================Load FP details=============== 
	public void loadFPDetails() throws Exception {

		ReadExcel ReadExl = new ReadExcel();
		DataLoader Loader = new DataLoader();
		ArrayList<Map<Integer, String>> SheetList = null;
		
		SheetList        = ReadExl.init(Propertymap.get("FixPackage.Excel.Path"));
		FP_InfoMap       = SheetList.get(0);
		FP_AirMap        = SheetList.get(1);
		FP_HotelMap      = SheetList.get(2);
		FP_ActivityMap   = SheetList.get(3);
		FP_ContentMap    = SheetList.get(4);
		FP_ItinaryMap    = SheetList.get(5);
		FP_HotelrateMap  = SheetList.get(6);
	    FP_ActRateMap    = SheetList.get(7);
	    
		if (Propertymap.get("FixPackage.Setup").trim().equalsIgnoreCase("enable")) {
		
        
			// Loading Hotel Details
			try {
				objlist = Loader.loadFPObjects(FP_InfoMap);
				logger.info("Basic Fp Objects Loaded SuccessFully");
				
			} catch (Exception e) {
				logger.fatal("Basic Fp Objects  not Loaded SuccessFully");
				logger.fatal(e.toString());
			}
			
			// Loading Air  Details
			try {
				objlist = Loader.loadFPAirInfo(FP_AirMap, objlist);
				logger.info("FP_Air Details Loaded SuccessFully");
				
			} catch (Exception e) {
				logger.info("FP_Air Details Not Loaded SuccessFully"); 
				logger.fatal(e.toString());
			}
	

			// Loading Hotel Details
			try {
				objlist = Loader.loadFPHotelInfo(FP_HotelMap, FP_HotelrateMap, objlist);
				logger.info("FP_Hotel Details Loaded SuccessFully");
			} catch (Exception e) {
				logger.info("FP_Hotel Details Not Loaded SuccessFully");
				logger.fatal(e.toString());
			}				
						
			// Loading Activity Details

			try {
				objlist = Loader.loadFPActivityInfo(FP_ActivityMap, FP_ActRateMap, objlist);
				logger.info("FP_Activity Details Loaded SuccessFully");
			} catch (Exception e) {
				logger.info("FP_Activity Details not Loaded SuccessFully");
				//logger.fatal(e.toString());
			}

			try {
				objlist = Loader.LoadFPItenaryDetails(FP_ItinaryMap, objlist);
				logger.info("FP_Itinary Loaded SuccessFully");
			} catch (Exception e) {
				logger.info("FP_Itinary not Loaded SuccessFully");
				logger.fatal(e.toString());
			}

			// Loading Tax Details

			try {
				objlist = Loader.LoadFpContent(FP_ContentMap, objlist);
				logger.info("FP_Content Loaded SuccessFully");
				
			} catch (Exception e) {
				logger.info("FP_Content Loaded not SuccessFully");
				logger.fatal(e.toString());
			}

		
		}

	}
   //====================Load Activity details=============== 
	public void loadactivitydetails() {
		
		
		
		ReadExcel ReadExl = new ReadExcel();
		
		
			
		ActivitySheetlist            = ReadExl.init(PG_Properties.getProperty("Excel.Path.FullActivity"));
		ActivitySheetlist1           = ReadExl.init(PG_Properties.getProperty("Excel.Path.BasicInfo"));
		
		detailsLoder         = new LoadActivityDetails();
		basicDetailsloader   = new LoadActivityBasicDetails(ActivitySheetlist1 );
		
		ProgramTypes   	 	= basicDetailsloader.loadProgramType();
		ActivityTypes 		= basicDetailsloader.loadActivitySetup();
		PeriodSetups 		= basicDetailsloader.loadPeriodSetup();
		RatePlans 			= basicDetailsloader.loadRatePlans();

		ActivityStandardMap 		= ActivitySheetlist.get(0);
		ActivityContractMap			= ActivitySheetlist.get(1);
		ActivityPoliciesMap			= ActivitySheetlist.get(2);
		ActivityTaxMap				= ActivitySheetlist.get(3);
		AssignActivityMap			= ActivitySheetlist.get(4); 
		ActivityInventoryMap		= ActivitySheetlist.get(5);
		ActivityRatesMap			= ActivitySheetlist.get(6);
		ActivityProfitMap			= ActivitySheetlist.get(7);
		
		
		try {		
			ActivityList = detailsLoder.loadActivityStandard(ActivityStandardMap);
					
		} catch (Exception e) {					
			System.out.println(e.getMessage());			
		}
		    
		
		try {			
			ActivityList = detailsLoder.loadActivityContract(ActivityContractMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {
			ActivityList = detailsLoder.loadActivityPolicies(ActivityPoliciesMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
			ActivityList = detailsLoder.loadActivityTax(ActivityTaxMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
				
		try {
			ActivityList = detailsLoder.loadActivityAssignActivity(AssignActivityMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		try {			
			ActivityList = detailsLoder.loadActivityInventories(ActivityInventoryMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {			
			ActivityList = detailsLoder.loadActivityRates(ActivityRatesMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {			
			ActivityList = detailsLoder.loadActivityProfits(ActivityProfitMap, ActivityList);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		
	}

	@Test
	public void testAvi() throws InterruptedException, NumberFormatException,
			ParseException, Exception {

		HotelSetupFlow Flow = new HotelSetupFlow(Propertymap);
		Flow.loginPage(driver);

		// Supplier Setup
		
		try {
			if (Propertymap.get("Supplier.Setup").trim()
					.equalsIgnoreCase("enable")) {
				SupplierSetupFlow supFlow = new SupplierSetupFlow();
				Iterator<Supplier> it = SupplierList.iterator();

				while (it.hasNext()) {
					Supplier supplier = (Supplier) it.next();
					supFlow.CreatePage(driver, supplier,
							Propertymap.get("Hotel.BaseUrl"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// Basic Setup
		
		try {
			if (Propertymap.get("Basic.Setup").trim()
					.equalsIgnoreCase("enable")) {
				BasicDriver.hotelSetup(driver);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//Currency setup method
		
		try {
			if (Propertymap.get("Currency.Setup").trim().equalsIgnoreCase("enable")) {
				
				SupplierSetupFlow supFlow = new SupplierSetupFlow();    
				Iterator<Currency> it = currencyList.iterator();				
				while (it.hasNext()) {
					Currency currency = it.next();
					supFlow.setCurrencyPageSetup(driver, currency, Propertymap.get("Hotel.BaseUrl"));				
				}
				
				Iterator<Currency> itExchange = currencyList.iterator();				
				while (itExchange.hasNext()) {
					Currency currency = itExchange.next();
					supFlow.setCurrencyExchangeRateSetup(driver, currency, Propertymap.get("Hotel.BaseUrl"));				
				}
							
			}
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
		
//  Booking Fee setup Creation- call to 
		try {
			if (Propertymap.get("BookingFee.Setup").trim().equalsIgnoreCase("enable")) {
				
				BookingFeeSetupFlow supFlow = new BookingFeeSetupFlow();    
				Iterator<HotelBookingFee> it = Bookingfeelist.iterator();				
				while (it.hasNext()) {
					HotelBookingFee bookingfee = it.next();
					supFlow.setBookingfeeSetup(driver,  bookingfee, Propertymap.get("Hotel.BaseUrl"));				
				}
				
							
				
							
			}
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
	// Air Configuration setup flow call	
		
		
		try {
			if (Propertymap.get("Airconfig.Setup").trim().equalsIgnoreCase("enable")) {
				
				AirConfigurationSetUpFlow supFlow = new AirConfigurationSetUpFlow();    
				Iterator<AirConfig> it = Airconfiglist.iterator();				
				while (it.hasNext()) {
					AirConfig airConfig = it.next();
					supFlow.setAirconfigSetup(driver,  airConfig, Propertymap.get("Hotel.BaseUrl"));				
				}
				
							
				
							
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (Propertymap.get("FixPackage.Setup").equalsIgnoreCase("enable")) {
				fixpackagesetupflow();
           } 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		//Discount setup method
		
		try {
			if (Propertymap.get("Discount.Setup").trim().equalsIgnoreCase("enable")) {
				
		        StringBuffer 	ReportPrinter 		= new StringBuffer();
				DiscountSetupFlow DisFlow = new DiscountSetupFlow();    
				Iterator<Discount> it = DiscountList.iterator();
				
				ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
				ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>3rd Party Hotel Reservations Report("+new SimpleDateFormat().format(Calendar.getInstance().getTime())+")</p></div>");
				ReportPrinter.append("<body>");
				
				while (it.hasNext()) {
					Discount discount = it.next();
				DisFlow.setUpDiscount(driver, discount , Propertymap.get("Hotel.BaseUrl"),ReportPrinter);				
				}
				
			}
							
		}
		 catch (Exception e) {
			// TODO: handle exception
		}	
		
		

	
  if (Propertymap.get("Hotel.Setup").trim().equalsIgnoreCase("enable")) {
		
		//Creating Screenshot Directory	
	    String                    screenpath              = "Screenshots/HotelSetup";
		File                      screenshotdir           = new File(screenpath);
		Iterator<Entry<String, Hotel>>           it       = HotelList.entrySet().iterator();		
		
		if (!screenshotdir.exists()) {
			logger.info("Screenshot path not available - Creating directories..");

			try {
				screenshotdir.mkdirs();
				logger.info("Directory created succcessfully!!");
			} catch (Exception e) {
				logger.fatal("Directory creation failed " + e.toString());
			}
		}
		
		while (it.hasNext()) {
			
			try {
                Hotel currentHotel = it.next().getValue();
				Flow.setHotel(currentHotel);
				logger.info("Current Hotel Name ---->"+currentHotel.getHotelName());
				logger.info("Current set up needed ---->"+currentHotel.isSetupNeeded());
				
				//if (currentHotel.isSetupNeeded()&& (Propertymap.get("Hotel.Setup").equalsIgnoreCase("enable"))) {
				if (currentHotel.isSetupNeeded()) {
					
					String   ScenarioUrl    = screenpath+"/"+currentHotel.getHotelName().trim();
					File     ScenarioPath   = new File(ScenarioUrl);
					Flow.setScenarioUrl(ScenarioUrl);
					 if(!ScenarioPath.exists()){
						logger.info("Screenshot path not available - Creating directories..");
						
						try {
						 ScenarioPath.mkdirs();
						 logger.info("Directory created succcessfully!!");
						 } catch (Exception e) {
						 logger.fatal("Directory creation failed "+ e.toString());
						}
					}
	             
					try {
	                     
						        driver = Flow.CreatePage(driver);
								driver = Flow.ContractPage(driver);
								driver = Flow.policyPage(driver);

								driver = Flow.OccupancyPage(driver);
								driver = Flow.addInventory(driver);
								driver = Flow.addRates(driver);

								
								
						
								
								
								try {
									driver = Flow.AmenitiesPage(driver);

								} catch (Exception e) {
									logger.fatal("Error when Adding Amenities"
											+ e.toString());
								}
								
								try {
									driver = Flow.FiltersPage(driver);

								} catch (Exception e) {
									logger.fatal("Error when Adding Filters"
											+ e.toString());
								}
								
								try {
									driver = Flow.ContentPage(driver);

								} catch (Exception e) {
									logger.fatal("Error when Adding Content"
											+ e.toString());
								}
								
								try {
									driver = Flow.ImagesPage(driver);

								} catch (Exception e) {
									logger.fatal("Error when Adding Images"
											+ e.toString());
								}

								if (currentHotel.getRateContract() != RateContractByType.COMMISSIONABLE) {
									logger.info("Hotel is non commissionable Adding Profit Mark up");
									driver = Flow.ProfiMarkUpPage(driver);
								} else {
									logger.info("Hotel is commissionable not Adding Profit Mark up");
								}
						
					
						
						
						if (Propertymap.get("Supplimentary.Enabled").equalsIgnoreCase("enable")) 
						driver = Flow.createSupplementarySetup(driver);
					
	                    if (Propertymap.get("Promotion.Enabled").equalsIgnoreCase("enable")) 
						driver = Flow.createPromotionSetup(driver);
					

					} catch (Exception e) {
						logger.fatal("Error with Hotel Setup of the hotel"
								+ currentHotel.getHotelName());
						logger.fatal(e.toString());
						Flow.takeScreenshot(ScenarioUrl+"/SetupFaliure.jpg", driver);
						{
							try {
								Flow.DeleteHotel(driver, currentHotel.getHotelName());
							} catch (Exception e2) {
								logger.warn("Hotel Deletion Failed",e2);
							}
						}
					}
	               
				}else{
					logger.info("Current Hotel Set Up not needed Skipping Set Up Flow ---->"+currentHotel.isSetupNeeded());
				}
			  } catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
		}
		
	} else {
		logger.info("Hotel Set up flow disabled Skipping setup");
	}
		
			
		activitySetup();

	}
	
	
	
public void activitySetup() throws InterruptedException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
		ActivityBasicInfo activityBasicInfo = new ActivityBasicInfo(driver);
		ActivitySetupInfo activitySetupInfo = new ActivitySetupInfo(driver);
		
		
	/*PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Activity Setup Report - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");*/
		
		
	//PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
	TestCaseCount = 1;
		

		
		//create activity basic info
		if (PG_Properties.getProperty("BasicInfoOperation").equals("Yes")) {
			
			//Programs
		/*	try {				
				Iterator<ProgramType>  programIterator  = ProgramTypes.iterator();			
				while(programIterator.hasNext())
				{
					activityBasicInfo.createProgramType(programIterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			*/
			
			//Activity type
			try {				
				Iterator<ActivityType>  activityiterator  = ActivityTypes.iterator();			
				while(activityiterator.hasNext())
				{
					activityBasicInfo.createActivityType(activityiterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			
			//Periods
			try {				
				Iterator<PeriodSetup>  periodSetupIterator  = PeriodSetups.iterator();			
				while(periodSetupIterator.hasNext())
				{
					activityBasicInfo.createSetupPeriod(periodSetupIterator.next());
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
			
			//Rate Plans
			try {
				Iterator<ActivityRatePlan>  rateIterator  = RatePlans.iterator();			
				while(rateIterator.hasNext())
				{
					activityBasicInfo.createRatePlan(rateIterator.next());
				}
					
			} catch (Exception e) {
				System.out.println(e.getMessage());	
			}
		}
		
		
		
		//create		
		if (PG_Properties.getProperty("ActivitySetupOperation").equals("Yes")) {
			
			Iterator<Map.Entry<String, ActivityStandard>> it = (Iterator<Entry<String, ActivityStandard>>) ActivityList.entrySet().iterator();
					
			while(it.hasNext())
			{
				
				ActivityStandard act = it.next().getValue();
				activitySetupInfo.addActivity(act);
				i++;
				
				TestCaseCount++;
		
				
				try {
					
					driver = activitySetupInfo.createActivityStandard(driver);
					driver = activitySetupInfo.addContracts(driver);
					driver = activitySetupInfo.addPolicies(driver);
					driver = activitySetupInfo.addContent(driver);
					driver = activitySetupInfo.addImages(driver);
					driver = activitySetupInfo.assignActivities(driver);
					driver = activitySetupInfo.addActivityInventories(driver);
					driver = activitySetupInfo.addActivityRates(driver);
					driver = activitySetupInfo.addProfitMarkups(driver);
					
					
					PrintWriter.append("<td>Created Activity - "+act.getActivity_Program_Name()+" - Done</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				} catch (Exception e) {
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
			        
				//	PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				//	PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			
			
		}
		
		//Delete
		if (PG_Properties.getProperty("DeleteOperation").equals("Yes")) {
				
			Iterator<Map.Entry<String, ActivityStandard>> it = (Iterator<Entry<String, ActivityStandard>>) ActivityList.entrySet().iterator();
			
			while(it.hasNext())
			{
				ActivityStandard act = it.next().getValue();
				i++;
				
				TestCaseCount++;
	//			PrintWriter.append("<tr> <td>"+TestCaseCount+"</td>  <td>Remove Activity program</td>");
	//			PrintWriter.append("<td>Activity program should be removed</td>");
				
				try {
					activitySetupInfo.deleteActivityStandard(act);
					PrintWriter.append("<td>Success..!!!</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
					
				} catch (Exception e) {
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
			        
		//			PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
			//		PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}				
			}
			
			
			
		}
				
//		PrintWriter.append("</table>");
	}
	
	
	

	
	public Map<String, String> loadProperties(String filepath) {

		Map<String, String> PropertyMap = new HashMap<String, String>();
		try {
			Properties prop = new Properties();
			FileReader fs = new FileReader(new File(filepath));
			prop.load(fs);

			for (String key : prop.stringPropertyNames()) {
				PropertyMap.put(key, prop.getProperty(key));
			}

		} catch (Exception e) {
			System.out.println("Error With Adding Properties to the Map");
			
		}
		return PropertyMap;
	}
	
	@After
	public void tearDown() throws Exception {
		driver.close();

	}


}
