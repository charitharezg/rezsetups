package setup.utill;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import bsh.This;
//import java.util.HashMap;

//import org.openqa.jetty.html.Break;


public class ExtentReportTemplate {
	
	static boolean			result			= false;
	static String			screenshotName	= "";
	static String			screenshotPath	= "";
	static String			portal			= "";
	ExtentReports			report			= null;
	
	
	public static String getScreenshotPath() {
		return screenshotPath;
	}

	public static void setScreenshotPath(String screenshotPath) {
		ExtentReportTemplate.screenshotPath = screenshotPath;
	}

	private static class SingletonHolder { 
	    private static final ExtentReportTemplate INSTANCE = new ExtentReportTemplate();
	}
	
	public static ExtentReportTemplate getInstance() {
	    return SingletonHolder.INSTANCE;
	}
	
	public void Initialize(ExtentReports report)
	{
		this.report = report;
		try {
			screenshotPath = PropertySingleton.getInstance().getProperty("ScreenshotPath");
			portal = ReadProperties.getPropertyInMap( PropertySingleton.getInstance().getProperty("PortalSpecificPropPath")).get("PortalName");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	 
	public void verifyTrue(String Expected, String Actual, String testDescription) {
		
		try {
			
			if(Actual.trim().equalsIgnoreCase(Expected.trim()))
				result = true;
			else
				result = false;
		} catch (Exception e) {
			result = false;
		}
		
		addtoReport(testDescription, Expected, Actual, result);
	}
	
	public void verifyArrayList(ArrayList<String> Expected, ArrayList<String> Actual, String testDescription)
	{
		try 
		{
			if(Expected.containsAll(Actual) || Actual.containsAll(Expected))
				result = true;
			else
				result = false;
		} catch (Exception e) {
			result = false;
		}
		//addtoReport(testDescription, Expected, Actual, result);
	}
	
	public void verifyContains(String Expected,String Actual,String testDescription){
	  	
		try {
			
			if((Actual.trim().contains(Expected.trim()))||(Expected.trim().contains(Actual.trim())))
				result = true;
		else
			result = false;
		} catch (Exception e) {
			result = false;
		}
		
		addtoReport(testDescription, Expected, Actual, result);
	}
	
	public void verifyEqualIgnoreCase(String Expected,String Actual,String testDescription){
	  	
		try {
			
			if((Actual.trim().equalsIgnoreCase(Expected.trim()))||(Expected.trim().equalsIgnoreCase(Actual.trim())))
				result = true;
		else
			result = false;
		} catch (Exception e) {
			result = false;
		}
		
		addtoReport(testDescription, Expected, Actual, result);
	}
	
	public void verifyTrue(double Expected,double Actual,String testDescription){
		
		
		try {
			if(Actual == Expected)
				result = true;
		else
			result = false;
		} catch (Exception e) {
			result = false;
		}
		
		addtoReport(testDescription, String.valueOf(Expected), String.valueOf(Actual), result);
	}
	
	public void verifyTrue(boolean Expected,boolean Actual,String testDescription)
	  {
	  	
		try {
			if(Actual == Expected)
				result = true;
		else
			result = false;
		} catch (Exception e) {
			result = false;
		}
		
		addtoReport(testDescription, String.valueOf(Expected), String.valueOf(Actual), result);
	}
	
	public void addtoReport(String testDescription, String expected, String actual, boolean result)
	{
		screenshotName = "Failed_"+testDescription.replace(" ", "_")+".jpeg";
		
		ExtentTest test = report.startTest(testDescription);
		
		test.log(LogStatus.INFO, "Expected : "+expected);
	   
		if(result){
			test.log(LogStatus.INFO, "Actual : "+actual);
			test.log(LogStatus.PASS, "PASS");
		}else {
			test.log(LogStatus.INFO, "Actual : "+actual);
			//ScreenShot takeShot = new ScreenShot();
			//String path = takeShot.getScreenShotPathOnFailure(testDescription);
			String path = takeScreenshot(testDescription);
			try {
				path = path.replace("\\rezrobot\\..", "");
			} catch (Exception e) {
				
			}
			System.out.println(path);
			test.log(LogStatus.FAIL, "FAIL", test.addScreenCapture(path));
		}
		
		report.endTest(test);
		
		report.flush();
		
	}
	
	public String takeScreenshot(String testDescription)
	{
		String path = "";
		String date = Calender.getToday("yyyy-MM-dd");
		path = screenshotPath+portal+"/"+date+"/"+testDescription+"_"+Calendar.getInstance().getTimeInMillis()+".jpg";
		
		try {
			File scrFile = ((TakesScreenshot)DriverFactory.getInstance().getDriver()).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(path));
		} catch (WebDriverException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new File(path).getAbsolutePath();
	}
	
	public void setSystemInfo(Map<String, String> sysInfo)
	{
		/*
		test.log(LogStatus.INFO, testDescription, "Expected : "+expected);
	   
		if(result){
			test.log(LogStatus.INFO, "Actual : "+actual);
			test.log(LogStatus.PASS, "PASS");
		}else {
			test.log(LogStatus.INFO, "Actual : "+actual);
			String path = takeScreenshot(testDescription);
			test.log(LogStatus.FAIL, "FAIL", test.addScreenCapture(path));
		}*/
		System.out.println(sysInfo.get("BrowserType"));
		report.addSystemInfo(sysInfo);
		report.flush();
		
	}
	
	public void setInfoTest(String tableHeader, Map<String, String> infoMap)
	{
		ExtentTest test = report.startTest(tableHeader);
		
		Iterator<Map.Entry<String, String>>   itr = infoMap.entrySet().iterator();
        
        while (itr.hasNext()) {
            Map.Entry<String,String> entry = (Map.Entry<String,String>) itr.next();
            
            test.log(LogStatus.INFO, entry.getKey() +"      = "+entry.getValue());
            //System.out.println("Key===>"+entry.getKey() +" Value===>"+entry.getValue());
            
        }

		report.endTest(test);
		report.flush();
	}
}

